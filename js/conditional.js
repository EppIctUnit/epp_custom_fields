/**
 * @file
 * Attaches Choices.js behaviors for the EPP Custom Fields module.
 */

(function (Drupal) {
  "use strict";

  Drupal.behaviors.epp_custom_fields_conditional = {
    attach: function (context, settings) {
      if (drupalSettings.epp_conditional?.length) {
        let conditions = drupalSettings.epp_conditional;
        setTimeout(function () {
          Drupal.behaviors.epp_custom_fields_conditional.conditionsEffect(
            conditions
          );
        }, 500);
        const nameElement = context.querySelector(
          "[name='field_internet_space[0][placeholder][]']"
        );
        if (nameElement) {
          nameElement.addEventListener("change", function () {
            setTimeout(function () {
              Drupal.behaviors.epp_custom_fields_conditional.conditionsEffect(
                conditions
              );
            }, 500);
          });
        }
      }
    },
    conditionsEffect: function (conditions) {
      conditions.forEach((condition) => {
        const fieldConditional = document.querySelector(
          `[name='${condition.name_field_condition}']`
        );
        const value = condition.value;
        const name = condition.name_item;
        const vocabulary = condition.vocabulary;
        if (fieldConditional) {
          const elements = document.querySelectorAll(
            `[name='${name}'] .conditional-vocabulary.${vocabulary}`
          );
          if (fieldConditional.value === value) {
            Drupal.behaviors.epp_custom_fields_conditional.show(elements);
          } else {
            Drupal.behaviors.epp_custom_fields_conditional.hide(elements);
          }
        } else {
          const elements = document.querySelectorAll(
            `[name='${name}'] .conditional-vocabulary:not(.${vocabulary})`
          );
          Drupal.behaviors.epp_custom_fields_conditional.hide(elements);
        }
      });
    },
    show: function (elements) {
      elements.prop("disabled", false);
      elements.removeAttr("disabled");
      elements.show();
    },
    hide: function (elements) {
      elements.prop("selected", false);
      elements.prop("disabled", true);
      elements.hide();
    },
  };
})(Drupal);
