/**
 * @file
 * Javascript for the map geocoder widget.
 */

(function (Drupal) {
  'use strict';

  Drupal.geolocation.widget.addWidgetProvider('epp_geolocation', 'GeolocationLeafletMapWidget');

})(Drupal);
