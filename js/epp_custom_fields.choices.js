/**
 * @file
 * Attaches Choices.js behaviors for the EPP Custom Fields module.
 */

(function (Drupal) {
  "use strict";

  Drupal.behaviors.epp_custom_fields = {
    attach: function (context, settings) {
      const choices = new Choices(".choices-enabled", {
        removeItemButton: true,
        searchEnabled: true,
        searchChoices: true,
        itemSelectText: "",
        shouldSort: false,
        noResultsText: "No results found",
      });
    },
  };
})(Drupal);
