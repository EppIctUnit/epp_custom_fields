<?php

namespace Drupal\epp_custom_fields\Plugin\EntityReferenceSelection;

use Drupal\node\Plugin\EntityReferenceSelection\NodeSelection;

/**
 * Provides specific access control for the node entity type.
 *
 * @EntityReferenceSelection(
 *   id = "default:mep_by_former_status",
 *   label = @Translation("Mep by former status selection"),
 *   entity_types = {"node"},
 *   group = "default",
 *   weight = 1
 * )
 */
class MepByFormerStatusSelection extends NodeSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    // Start with the parent's query.
    $query = parent::buildEntityQuery($match, $match_operator);

    // Retrieve filter settings from configuration.
    $filter_settings = $this->configuration['filter'] ?? [];

    // Apply all filters except for the field_status.former filter.
    // We will enforce that condition and sorting logic afterward.
    foreach ($filter_settings as $field_name => $value) {
      if ($field_name !== 'field_status.former') {
        $query->condition($field_name, $value, '=');
      }
    }

    // Enforce the condition that field_status.former must be in [0,1].
    // Then sort by this field so that 0 values appear first, followed by 1 values.
    $query->condition('field_status.former', [0, 1], 'IN');
    $query->sort('field_status.former', 'ASC');

    return $query;
  }

}
