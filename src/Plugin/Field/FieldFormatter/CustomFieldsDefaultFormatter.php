<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'custom_fields_default_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "custom_fields_default_formatter",
 *   label = @Translation("Custom fields default formatter"),
 *   field_types = {
 *     "epp_contact",
 *     "epp_descriptions",
 *     "epp_dictionary",
 *     "epp_formatted_value",
 *     "epp_free_text_item",
 *     "epp_further_reading",
 *     "epp_mep_involved",
 *     "epp_mep_role",
 *     "epp_mep_status",
 *     "epp_metadata",
 *     "eep_names",
 *     "epp_picture",
 *     "epp_publish",
 *     "epp_socials",
 *     "epp_staff_service",
 *     "eep_vacancy",
 *     "mep_title",
 *     "mep_website",
 *   }
 * )
 */
class CustomFieldsDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = $this->t('Default formatter for custom fields.');
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    return ['#markup' => ''];
  }

}
