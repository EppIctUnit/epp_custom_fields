<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Entity\TypedData\EntityDataDefinition;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataReferenceDefinition;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;

/**
 * Provides a field type for EPP Alert User Data field.
 *
 * @FieldType(
 *   id = "epp_alert_user_data",
 *   label = @Translation("EPP Alert User Data"),
 *   category = @Translation("EPP"),
 *   default_formatter = "custom_fields_default_formatter",
 *   default_widget = "epp_alert_user_data_widget",
 * )
 */
class EPPAlertUserData extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
             'user_target_type' => 'user',
           ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
             'user' => [
               'handler'          => 'default',
               'handler_settings' => [],
             ],
           ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    /*$element['user_target_type'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Role code : type of entity to reference'),
      '#options'       => \Drupal::service('entity_type.repository')
                                 ->getEntityTypeLabels(TRUE),
      '#default_value' => $this->getSetting('user_target_type'),
      '#required'      => TRUE,
      '#disabled'      => $has_data,
      '#size'          => 1,
    ];*/

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'target_id'        => [
        'type'     => 'int',
        'length'   => 10,
        'not null' => TRUE,
        'unsigned' => TRUE,
      ],
      'is_open'          => [
        'type'     => 'int',
        'length'   => 10,
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default'  => 1,
      ],
      'discarded'        => [
        'type'     => 'int',
        'length'   => 10,
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default'  => 0,
      ],
      'executed'         => [
        'type'     => 'int',
        'length'   => 10,
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default'  => 0,
      ],
      'execution_time'   => [
        'type'     => 'int',
        'length'   => 10,
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default'  => 0,
      ],
      'action_result'    => [
        'type'     => 'varchar',
        'length'   => 255,
        'not null' => FALSE,
      ],
      'displayed_number' => [
        'type'     => 'int',
        'length'   => 10,
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default'  => 0,
      ],
    ];

    $indexes = [
      'search'   => ['target_id', 'is_open'],
      'user'     => ['target_id'],
      'executed' => ['executed', 'discarded'],
    ];

    return [
      'columns' => $columns,
      'indexes' => $indexes,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['target_id'] = DataReferenceTargetDefinition::create('integer')
                                                            ->setLabel("User name")
                                                            ->setSetting('unsigned', TRUE);

    // This is the definition of the reference target, it is needed but won't
    // appear as a field or a column in our table.
    $settings         = $field_definition->getSettings();
    $target_type_info = \Drupal::entityTypeManager()
                               ->getDefinition($settings['user_target_type']);

    $properties['referenced_entity'] = DataReferenceDefinition::create('entity')
                                                              ->setLabel($target_type_info->getLabel())
                                                              ->setDescription(t('@field referenced entity', ['@field' => "User name"]))
                                                              ->setComputed(TRUE)
                                                              ->setReadOnly(FALSE)
                                                              ->setTargetDefinition(EntityDataDefinition::create($settings['user_target_type']))
                                                              ->addConstraint('EntityType', $settings['user_target_type']);

    $properties['is_open'] = DataDefinition::create('integer')
                                           ->setLabel(t('Is still opened'));

    $properties['discarded'] = DataDefinition::create('integer')
                                             ->setLabel(t('Has been discarded'));

    $properties['executed'] = DataDefinition::create('integer')
                                            ->setLabel(t('Has been executed'));

    $properties['execution_time'] = DataDefinition::create('integer')
                                                  ->setLabel(t('Execution time'));

    $properties['action_result'] = DataDefinition::create('string')
                                                 ->setLabel(t('Results'));

    $properties['displayed_number'] = DataDefinition::create('integer')
                                                    ->setLabel(t('How many time displayed'));


    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return (empty($this->get('target_id')
                       ->getValue()));
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    return [
      'target_id'        => 0,
      'is_open'          => 0,
      'discarded'        => 0,
      'executed'         => 0,
      'execution_time'   => 0,
      'action_result'    => 'Stub',
      'displayed_number' => 0,
    ];
  }

}
