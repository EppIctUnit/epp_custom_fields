<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Provides a field type for EPP Contact field.
 *
 * @FieldType(
 *   id = "epp_contact",
 *   label = @Translation("EPP Contact"),
 *   category = @Translation("EPP"),
 *   default_formatter = "custom_fields_default_formatter",
 *   default_widget = "epp_contact_widget",
 * )
 */
class EPPContact extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'location' => [
        'type' => 'varchar',
        'length' => 512,
        'not null' => FALSE,
      ],
      'office' => [
        'type' => 'varchar',
        'length' => 50,
        'not null' => FALSE,
      ],
      'address' => [
        'type' => 'varchar',
        'length' => 512,
        'not null' => FALSE,
      ],
      'phone' => [
        'type' => 'varchar',
        'length' => 30,
        'not null' => FALSE,
      ],
      'mobile' => [
        'type' => 'varchar',
        'length' => 30,
        'not null' => FALSE,
      ],
      'fax' => [
        'type' => 'varchar',
        'length' => 30,
        'not null' => FALSE,
      ],
    ];

    $indexes = [
      'location' => ['location'],
      'office' => ['office'],
    ];

    return [
      'columns' => $columns,
      'indexes' => $indexes,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['location'] = DataDefinition::create('string')
      ->setLabel(t('Location'));

    $properties['office'] = DataDefinition::create('string')
      ->setLabel(t('Office'));

    $properties['address'] = DataDefinition::create('string')
      ->setLabel(t('Address'));

    $properties['phone'] = DataDefinition::create('string')
      ->setLabel(t('Phone'));

    $properties['mobile'] = DataDefinition::create('string')
      ->setLabel(t('Mobile'));

    $properties['fax'] = DataDefinition::create('string')
      ->setLabel(t('Fax'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->get('location')->getValue())
      && empty($this->get('office')->getValue())
      && empty($this->get('address')->getValue())
      && empty($this->get('phone')->getValue())
      && empty($this->get('mobile')->getValue())
      && empty($this->get('fax')->getValue());
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    return [
      'location' => 'Stub',
      'office' => 'Stub',
      'address' => 'Stub',
      'phone' => 'Stub',
      'mobile' => 'Stub',
      'fax' => 'Stub',
    ];
  }

}
