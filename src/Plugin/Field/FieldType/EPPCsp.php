<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Provides a field type for EPP Contact field.
 *
 * @FieldType(
 *   id = "epp_csp",
 *   label = @Translation("EPP Csp"),
 *   category = @Translation("EPP"),
 *   default_formatter = "custom_fields_default_formatter",
 *   default_widget = "epp_csp_widget",
 * )
 */
class EPPCsp extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'type' => [
        'type' => 'varchar',
        'length' => 512,
        'not null' => FALSE,
      ],
      'api' => [
        'type' => 'varchar',
        'length' => 2048,
        'not null' => FALSE,
      ],
    ];

    $indexes = [
      'type' => ['type'],
      'api' => ['api'],
    ];

    return [
      'columns' => $columns,
      'indexes' => $indexes,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['type'] = DataDefinition::create('string')
      ->setLabel(t('Type'));
    $properties['api'] = DataDefinition::create('string')
      ->setLabel(t('Api Url'));
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->get('api')->getValue());
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    return [
      'type' => 'Stub',
      'api' => 'Stub',
    ];
  }

}
