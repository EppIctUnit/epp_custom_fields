<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Provides a field type for EPP Contact field.
 *
 * @FieldType(
 *   id = "epp_descriptions",
 *   label = @Translation("EPP Descriptions"),
 *   category = @Translation("EPP"),
 *   default_formatter = "custom_fields_default_formatter",
 *   default_widget = "epp_descriptions_widget",
 * )
 */
class EPPDescriptions extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'short' => [
        'type' => 'varchar',
        'length' => 2048,
        'not null' => FALSE,
      ],
      'long' => [
        'type' => 'varchar',
        'length' => 2048,
        'not null' => FALSE,
      ],
      'mobile' => [
        'type' => 'varchar',
        'length' => 2048,
        'not null' => FALSE,
      ],
    ];

    $indexes = [];

    return [
      'columns' => $columns,
      'indexes' => $indexes,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['short'] = DataDefinition::create('string')
      ->setLabel(t('Short description'));

    $properties['long'] = DataDefinition::create('string')
      ->setLabel(t('Long description'));

    $properties['mobile'] = DataDefinition::create('string')
      ->setLabel(t('Description for mobile'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->get('short')->getValue())
      && empty($this->get('long')->getValue())
      && empty($this->get('mobile')->getValue());
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    return [
      'short' => 'Stub',
      'long' => 'Stub',
      'mobile' => 'Stub',
    ];
  }

}
