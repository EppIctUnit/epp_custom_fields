<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Entity\TypedData\EntityDataDefinition;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataReferenceDefinition;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;

/**
 * Provides a EPP Dictionary field type.
 *
 * @FieldType(
 *   id = "epp_dictionary",
 *   label = @Translation("EPP Dictionary"),
 *   description = @Translation("Stores the MEP title information"),
 *   default_formatter = "custom_fields_default_formatter",
 *   default_widget = "epp_dictionary_default_widget",
 *   category = @Translation("EPP"),
 * )
 */
class EPPDictionary extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'target_type' => 'taxonomy_term',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'handler' => 'default',
      'handler_settings' => [],
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    $element['target_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type of entity to reference'),
      '#options' => \Drupal::service('entity_type.repository')
        ->getEntityTypeLabels(TRUE),
      '#default_value' => $this->getSetting('target_type'),
      '#required' => TRUE,
      '#disabled' => $has_data,
      '#size' => 1,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::fieldSettingsForm($form, $form_state);
    $settings = $this->getSettings();

    // Get the bundle options.
    $options = [];
    $bundle_options = \Drupal::service('entity_type.bundle.info')
      ->getBundleInfo($settings['target_type']);
    foreach ($bundle_options as $key => $option) {
      $options[$key] = $option['label'];
    }

    $form['handler_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Entity reference settings'),
      '#attributes' => ['class' => ['entity_reference-settings']],
    ];

    $form['handler_settings']['target_bundles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Bundles'),
      '#options' => $options,
      '#default_value' => $settings['handler_settings']['target_bundles'] ?? [],
      '#description' => $this->t('Bundles for the entity reference field'),
      '#required' => TRUE,
    ];

    $form['handler_settings']['access_delegation_container'] = [
      '#type' => 'details',
      '#title' => $this->t('Countries options'),
      '#open' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="settings[handler_settings][target_bundles][countries]"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];
    $form['handler_settings']['access_delegation_container']['access_delegation'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Access delegation only'),
      '#default_value' => $settings['handler_settings']['access_delegation_container']['access_delegation'] ?? 0,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'stid' => [
        'type' => 'varchar',
        'length' => 15,
        'not null' => TRUE,
      ],
      'target_id' => [
        'type' => 'int',
        'length' => 10,
        'not null' => TRUE,
        'unsigned' => TRUE,
      ],
    ];

    $indexes = [
      'stid' => ['stid'],
    ];

    return [
      'columns' => $columns,
      'indexes' => $indexes,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Entity reference fields definition.
    // This will be the entity_reference field itself.
    $properties['target_id'] = DataReferenceTargetDefinition::create('integer')
      ->setLabel(t('Term reference'))
      ->setSetting('unsigned', TRUE)
      ->setRequired(TRUE)
      ->addConstraint('NotNull');

    // This is the definition of the reference target, it is needed but won't
    // appear as a field or a column in our table.
    $settings = $field_definition->getSettings();
    $target_type_info = \Drupal::entityTypeManager()
      ->getDefinition($settings['target_type']);

    $properties['referenced_entity'] = DataReferenceDefinition::create('entity')
      ->setLabel($target_type_info->getLabel())
      ->setDescription(t('The referenced entity'))
      ->setComputed(TRUE)
      ->setReadOnly(FALSE)
      ->setTargetDefinition(EntityDataDefinition::create($settings['target_type']))
      ->addConstraint('EntityType', $settings['target_type'])
      ->setRequired(TRUE)
      ->addConstraint('NotNull');

    // @todo get bundles (not in $settings).
    if (isset($settings['handler_settings']['target_bundles']) && !empty($settings['handler_settings']['target_bundles'])) {
      $properties['referenced_entity']->addConstraint('Bundle', $settings['handler_settings']['target_bundles']);
    }

    // Classic fields definition.
    $properties['stid'] = DataDefinition::create('string')
      ->setLabel(t('Dictionary name reference'))
      ->addConstraint('NotNull')
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    // Since all the columns are 'not null', require all the values.
    if (empty($this->get('stid')->getValue())
      || empty($this->get('target_id')->getValue())) {
      return TRUE;
    }
    return FALSE;
  }

}
