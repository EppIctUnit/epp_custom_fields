<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\text\Plugin\Field\FieldType\TextLongItem;

/**
 * Provides a field type for EPP Formatted Value field.
 *
 * @FieldType(
 *   id = "epp_formatted_value",
 *   label = @Translation("EPP Formatted Value"),
 *   category = @Translation("EPP"),
 *   default_formatter = "custom_fields_default_formatter",
 *   default_widget = "text_textarea",
 * )
 */
class EPPFormattedValue extends TextLongItem {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);

    $schema['columns']['safe_value'] = [
      'type' => 'text',
      'size' => 'big',
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['safe_value'] = DataDefinition::create('string')
      ->setLabel(t('Safe value'));

    return $properties;
  }

}
