<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type for EPP Contact field.
 *
 * @FieldType(
 *   id = "epp_free_text_item",
 *   label = @Translation("EPP Free Text item"),
 *   category = @Translation("EPP"),
 *   default_formatter = "custom_fields_default_formatter",
 *   default_widget = "epp_free_text_item_widget",
 * )
 */
class EPPFreeTextItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'description' => [
        'type' => 'varchar',
        'length' => 150,
        'not null' => FALSE,
      ],
      'text' => [
        'type' => 'blob',
        'size' => 'big',
        'not null' => FALSE,
      ],
      'text_format' => [
        'type' => 'varchar_ascii',
        'length' => 255,
        'not null' => FALSE,
      ],
    ];

    $indexes = [
      'description' => ['description'],
    ];

    return [
      'columns' => $columns,
      'indexes' => $indexes,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['description'] = DataDefinition::create('string')
      ->setLabel(t('Description'));

    $properties['text'] = DataDefinition::create('string')
      ->setLabel(t('Text'));

    $properties['text_format'] = DataDefinition::create('filter_format')
      ->setLabel(t('Text format'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->get('text')->getValue());
  }

}
