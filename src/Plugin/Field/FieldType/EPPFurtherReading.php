<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type for EPP Further reading field.
 *
 * @FieldType(
 *   id = "epp_further_reading",
 *   label = @Translation("EPP Further reading"),
 *   category = @Translation("EPP"),
 *   default_formatter = "custom_fields_default_formatter",
 *   default_widget = "epp_further_reading_widget",
 * )
 */
class EPPFurtherReading extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'reading_text' => [
        'type' => 'varchar',
        'length' => 150,
        'not null' => FALSE,
      ],
      'reading_link' => [
        'type' => 'varchar',
        'length' => 1000,
        'not null' => FALSE,
      ],
      'target' => [
        'type' => 'varchar',
        'length' => 20,
        'not null' => FALSE,
      ],
    ];

    $indexes = [];

    return [
      'columns' => $columns,
      'indexes' => $indexes,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['reading_text'] = DataDefinition::create('string')
      ->setLabel(t('Further reading title'));

    $properties['reading_link'] = DataDefinition::create('string')
      ->setLabel(t('Further reading link'));

    $properties['target'] = DataDefinition::create('string')
      ->setLabel(t('Target'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->get('reading_text')->getValue());
  }

}
