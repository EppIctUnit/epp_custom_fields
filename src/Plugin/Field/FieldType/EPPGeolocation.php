<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'epp_geolocation' field type.
 *
 * @FieldType(
 *   id = "epp_geolocation",
 *   label = @Translation("EPP Geolocation"),
 *   category = @Translation("EPP"),
 *   description = @Translation("This field stores location data (lat, lng)."),
 *   default_widget = "epp_geolocation",
 *   default_formatter = "geolocation_latlng"
 * )
 */
final class EPPGeolocation extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    $lat = $this->get('lat')->getValue();
    $lng = $this->get('lng')->getValue();
    return $lat === NULL || $lat === '' || $lng === NULL || $lng === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {

    $properties['lat'] = DataDefinition::create('float')
      ->setLabel(t('Latitude'));

    $properties['lng'] = DataDefinition::create('float')
      ->setLabel(t('Longitude'));

    $properties['content'] = DataDefinition::create('string')
      ->setLabel(t('Content'));

    $properties['content_format'] = DataDefinition::create('filter_format')
      ->setLabel(t('Content format'));

    $properties['address_display'] = DataDefinition::create('string')
      ->setLabel(t('Address display'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    $columns = [
      'lat' => [
        'description' => 'Stores the latitude value',
        'type' => 'float',
        'size' => 'big',
        'not null' => TRUE,
      ],
      'lng' => [
        'description' => 'Stores the longitude value',
        'type' => 'float',
        'size' => 'big',
        'not null' => TRUE,
      ],
      'address_display' => [
        'type' => 'varchar_ascii',
        'length' => 255,
        'default' => NULL,
      ],
      'content' => [
        'type' => 'text',
        'size' => 'big',
        'default' => NULL,
      ],
      'content_format' => [
        'type' => 'varchar_ascii',
        'length' => 255,
        'default' => NULL,
      ],
    ];

    $schema = [
      'columns' => $columns,
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition): array {
    $values['lat'] = (float) rand(-89, 90) - rand(0, 999999) / 1000000;
    $values['lng'] = (float) rand(-179, 180) - rand(0, 999999) / 1000000;
    return $values;
  }

}
