<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Entity\TypedData\EntityDataDefinition;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataReferenceDefinition;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;

/**
 * Provides a Epp Mep involved field type.
 *
 * @FieldType(
 *   id = "epp_mep_involved",
 *   label = @Translation("Epp Mep involved"),
 *   description = @Translation("Stores the Epp Mep involved information"),
 *   default_formatter = "custom_fields_default_formatter",
 *   default_widget = "epp_mep_involved_widget",
 *   category = @Translation("EPP"),
 * )
 */
class EPPMepInvolved extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'role_target_type' => 'taxonomy_term',
      'node_target_type' => 'node',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'role' => [
        'handler' => 'default',
        'handler_settings' => [
          'target_bundles' => [
            'meps_involved_roles' => 'meps_involved_roles',
          ],
        ],
      ],
      'node' => [
        'handler' => 'default',
        'handler_settings' => [
          'target_bundles' => [
            'mep' => 'mep',
          ],
        ],
      ],
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    $element['role_target_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Role code : type of entity to reference'),
      '#options' => \Drupal::service('entity_type.repository')
        ->getEntityTypeLabels(TRUE),
      '#default_value' => $this->getSetting('role_target_type'),
      '#required' => TRUE,
      '#disabled' => $has_data,
      '#size' => 1,
    ];

    $element['node_target_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Node type: type of entity to reference'),
      '#options' => \Drupal::service('entity_type.repository')
        ->getEntityTypeLabels(TRUE),
      '#default_value' => $this->getSetting('node_target_type'),
      '#required' => TRUE,
      '#disabled' => $has_data,
      '#size' => 1,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::fieldSettingsForm($form, $form_state);
    $settings = $this->getSettings();

    $fields = [
      'role' => $this->t("MEP's Role"),
      'node' => $this->t("MEP's Name"),
    ];

    foreach ($fields as $machine_name => $field_label) {
      // Get the reference target entity type from the storage settings.
      $target_type = $settings[$machine_name . '_target_type'];

      // Get the bundle options.
      $options = [];
      $bundle_options = \Drupal::service('entity_type.bundle.info')
        ->getBundleInfo($target_type);
      foreach ($bundle_options as $key => $option) {
        $options[$key] = $option['label'];
      }

      $form[$machine_name]['handler_settings'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('@label entity reference settings', ['@label' => $field_label]),
        '#attributes' => ['class' => ['entity_reference-settings']],
      ];

      $form[$machine_name]['handler_settings']['target_bundles'] = [
        '#type' => 'checkboxes',
        '#options' => $options,
        '#title' => $this->t('Bundles'),
        '#default_value' => $settings[$machine_name]['handler_settings']['target_bundles'],
        '#description' => $this->t('@label entity reference bundles', ['@label' => $field_label]),
        '#required' => TRUE,
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns['role_code'] = [
      'type' => 'varchar',
      'length' => 50,
      'not null' => FALSE,
    ];
    $columns['role_target_id'] = [
      'type' => 'int',
      'length' => 10,
      'not null' => FALSE,
      'unsigned' => TRUE,
    ];
    $columns['node_target_id'] = [
      'type' => 'int',
      'length' => 10,
      'not null' => TRUE,
      'unsigned' => TRUE,
    ];
    $indexes = [
      'node_target_id' => ['node_target_id'],
      'role_target_id' => ['role_target_id'],
    ];

    return [
      'columns' => $columns,
      'indexes' => $indexes,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['role_code'] = DataDefinition::create('string')
      ->setLabel(t("MEP's Role :"));
    // Entity reference fields definition.
    $fields = [
      'role' => t("MEP's Role term reference"),
      'node' => t("MEP's Name"),
    ];

    foreach ($fields as $machine_name => $field_label) {
      // This will be the entity_reference field itself.
      $properties[$machine_name . '_target_id'] = DataReferenceTargetDefinition::create('integer')
        ->setLabel($field_label)
        ->setSetting('unsigned', TRUE);

      // This is the definition of the reference target, it is needed but won't
      // appear as a field or a column in our table.
      $settings = $field_definition->getSettings();
      $target_type_info = \Drupal::entityTypeManager()
        ->getDefinition($settings[$machine_name . '_target_type']);

      $properties[$machine_name . '_referenced_entity'] = DataReferenceDefinition::create('entity')
        ->setLabel($target_type_info->getLabel())
        ->setDescription(t('@field referenced entity', ['@field' => $field_label]))
        ->setComputed(TRUE)
        ->setReadOnly(FALSE)
        ->setTargetDefinition(EntityDataDefinition::create($settings[$machine_name . '_target_type']))
        ->addConstraint('EntityType', $settings[$machine_name . '_target_type'])
        ->addConstraint('Bundle', [$machine_name]);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    if (empty($this->get('node_target_id')->getValue())) {
      return TRUE;
    }
    return FALSE;
  }

}
