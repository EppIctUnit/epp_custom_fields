<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type for EPP MEP Role field.
 *
 * @FieldType(
 *   id = "epp_mep_role",
 *   label = @Translation("EPP MEP Role"),
 *   category = @Translation("EPP"),
 *   default_formatter = "custom_fields_default_formatter",
 *   default_widget = "epp_mep_role_widget",
 * )
 */
class EPPMepRole extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'code' => [
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
      ],
      'name' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ],
      'weight' => [
        'type' => 'int',
        'length' => 10,
        'unsigned' => FALSE,
        'not null' => FALSE,
      ],
    ];

    $indexes = [
      'code' => ['code'],
    ];

    return [
      'columns' => $columns,
      'indexes' => $indexes,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['code'] = DataDefinition::create('string')
      ->setLabel(t('Role code'));

    $properties['name'] = DataDefinition::create('string')
      ->setLabel(t('Role name'));

    $properties['weight'] = DataDefinition::create('integer')
      ->setLabel(t('Role precedence'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->get('code')->getValue())
      || empty($this->get('name')->getValue())
      || (empty($this->get('weight')->getValue()) && ($this->get('weight')->getValue() != 0));
  }

}
