<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Provides a field type for EPP MEP Status field.
 *
 * @FieldType(
 *   id = "epp_mep_status",
 *   label = @Translation("EPP MEP Status"),
 *   category = @Translation("EPP"),
 *   default_formatter = "custom_fields_default_formatter",
 *   default_widget = "epp_mep_status_widget",
 * )
 */
class EPPMepStatus extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'presidence_member' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'presidence_function' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ],
      'bureau_member' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'bureau_function' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ],
      'nat_del_head' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'former' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'nat_del_weight' => [
        'type' => 'int',
        'unsigned' => FALSE,
        'not null' => TRUE,
        'default' => 0,
      ],
    ];

    $indexes = [
      'presm' => ['presidence_member'],
      'burm' => ['bureau_member'],
      'ndhead' => ['nat_del_head'],
      'former' => ['former'],
    ];

    return [
      'columns' => $columns,
      'indexes' => $indexes,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['presidence_member'] = DataDefinition::create('integer')
      ->setLabel(t('Is Presidence Member'));

    $properties['presidence_function'] = DataDefinition::create('string')
      ->setLabel(t('Function in Presidence'));

    $properties['bureau_member'] = DataDefinition::create('integer')
      ->setLabel(t('Is Bureau Member'));

    $properties['bureau_function'] = DataDefinition::create('string')
      ->setLabel(t('Function in Bureau'));

    $properties['nat_del_head'] = DataDefinition::create('integer')
      ->setLabel(t('Is Head of National Delegation'));

    $properties['former'] = DataDefinition::create('integer')
      ->setLabel(t('Is Former Member'));

    $properties['nat_del_weight'] = DataDefinition::create('string')
      ->setLabel(t('Precedence in National Delegation'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return (empty($this->get('nat_del_weight')->getValue()) && ($this->get('nat_del_weight')->getValue() != 0));
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    return [
      'presidence_member' => 0,
      'presidence_function' => 'Stub',
      'bureau_member' => 0,
      'bureau_function' => 'Stub',
      'nat_del_head' => 0,
      'former' => 0,
      'nat_del_weight' => 'Stub',
    ];
  }

}
