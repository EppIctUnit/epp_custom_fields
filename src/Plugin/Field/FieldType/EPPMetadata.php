<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Provides a field type for EPP Metadata field.
 *
 * @FieldType(
 *   id = "epp_metadata",
 *   label = @Translation("EPP Metadata"),
 *   category = @Translation("EPP"),
 *   default_formatter = "custom_fields_default_formatter",
 *   default_widget = "epp_metadata_widget",
 * )
 */
class EPPMetadata extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'body_code' => [
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
      ],
      'body_weight' => [
        'type' => 'int',
        'length' => 10,
        'unsigned' => FALSE,
        'not null' => FALSE,
      ],
      'bodytype_code' => [
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
      ],
      'bodytype_name' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ],
      'bodytype_weight' => [
        'type' => 'int',
        'length' => 10,
        'unsigned' => FALSE,
        'not null' => FALSE,
      ],
    ];

    $indexes = [
      'b_code' => ['body_code'],
      'bt_code' => ['bodytype_code'],
    ];

    return [
      'columns' => $columns,
      'indexes' => $indexes,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['body_code'] = DataDefinition::create('string')
      ->setLabel(t('Body code'))
      ->addConstraint('NotNull')
      ->setRequired(TRUE);

    $properties['body_weight'] = DataDefinition::create('integer')
      ->setLabel(t('Body precedence'))
      ->setRequired(FALSE);

    $properties['bodytype_code'] = DataDefinition::create('string')
      ->setLabel(t('Body type code'))
      ->addConstraint('NotNull')
      ->setRequired(TRUE);

    $properties['bodytype_name'] = DataDefinition::create('string')
      ->setLabel(t('Body type name'))
      ->addConstraint('NotNull')
      ->setRequired(TRUE);

    $properties['bodytype_weight'] = DataDefinition::create('integer')
      ->setLabel(t('Body type precedence'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->get('body_code')->getValue())
      || empty($this->get('bodytype_code')->getValue());
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    return [
      'body_code' => 'Stub',
      'body_weight' => 0,
      'bodytype_code' => 'Stub',
      'bodytype_name' => 'Stub',
      'bodytype_weight' => 0,
    ];
  }

}
