<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Provides a field type for EPP Names field.
 *
 * @FieldType(
 *   id = "epp_names",
 *   label = @Translation("EPP Names"),
 *   description = @Translation("Store the first, last and full names"),
 *   category = @Translation("EPP"),
 *   default_formatter = "custom_fields_default_formatter",
 *   default_widget = "epp_names_widget",
 * )
 */
class EPPNames extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'first' => [
        'type' => 'varchar',
        'length' => 150,
        'not null' => TRUE,
      ],
      'last' => [
        'type' => 'varchar',
        'length' => 150,
        'not null' => TRUE,
      ],
      'full' => [
        'type' => 'varchar',
        'length' => 300,
        'not null' => TRUE,
      ],
      'sort' => [
        'type' => 'varchar',
        'length' => 150,
        'not null' => FALSE,
      ],
    ];

    return [
      'columns' => $columns,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['first'] = DataDefinition::create('string')
      ->setLabel(t('First name'))
      ->setRequired(TRUE);

    $properties['last'] = DataDefinition::create('string')
      ->setLabel(t('Last name'))
      ->setRequired(TRUE);

    $properties['full'] = DataDefinition::create('string')
      ->setLabel(t('Full name'))
      ->setRequired(TRUE);

    $properties['sort'] = DataDefinition::create('string')
      ->setLabel(t('Sort name'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->get('first')->getValue())
      && empty($this->get('last')->getValue())
      && empty($this->get('full')->getValue());
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    return [
      'first' => 'Stub',
      'last' => 'Stub',
      'full' => 'Stub',
      'sort' => 'Stub',
    ];
  }

}
