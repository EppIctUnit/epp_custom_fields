<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Entity\TypedData\EntityDataDefinition;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataReferenceDefinition;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;

/**
 * Provides a field type for EPP Picture field.
 *
 * @FieldType(
 *   id = "epp_picture",
 *   label = @Translation("EPP Picture"),
 *   category = @Translation("EPP"),
 *   default_formatter = "custom_fields_default_formatter",
 *   default_widget = "epp_picture_widget",
 *   list_class = "\Drupal\epp_custom_fields\Plugin\Field\FieldType\EppPictureFieldItemList",
 * )
 */
class EPPPicture extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'target_type' => 'node',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'handler' => 'default',
      'handler_settings' => [
        'target_bundles' => [
          'photo' => 'photo',
        ],
      ],
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    $element['target_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type of entity to reference'),
      '#options' => \Drupal::service('entity_type.repository')
        ->getEntityTypeLabels(TRUE),
      '#default_value' => $this->getSetting('target_type'),
      '#required' => TRUE,
      '#disabled' => $has_data,
      '#size' => 1,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::fieldSettingsForm($form, $form_state);
    $settings = $this->getSettings();

    // Get the reference target entity type from the storage settings.
    $target_type = $this->getSetting('target_type');

    // Get the bundle options.
    $options = [];
    $bundle_options = \Drupal::service('entity_type.bundle.info')
      ->getBundleInfo($target_type);
    foreach ($bundle_options as $key => $option) {
      $options[$key] = $option['label'];
    }

    // Make sure our default value is not erased on submit.
    $form['handler'] = [
      '#type' => 'hidden',
      '#value' => $settings['handler'] ?? 'default',
    ];

    $form['handler_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Entity reference settings'),
    ];
    $form['handler_settings']['target_bundles'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#title' => $this->t('Bundles'),
      '#default_value' => $settings['handler_settings']['target_bundles'],
      '#description' => $this->t('Bundles for the entity reference field'),
      '#required' => TRUE,
    ];

    foreach ($options as $bundle_machine_name => $bundle_label) {
      $fields = array_keys(\Drupal::service('entity_field.manager')->getFieldDefinitions($target_type, $bundle_machine_name));
      $form['handler_settings'][$bundle_machine_name . '_alt_field'] = [
        '#type' => 'select',
        '#options' => array_combine($fields, $fields),
        '#title' => $this->t('@bundle alt field', ['@bundle' => $bundle_label]),
        '#default_value' => !empty($settings['handler_settings'][$bundle_machine_name . '_alt_field'])
        ? $settings['handler_settings'][$bundle_machine_name . '_alt_field']
        : NULL,
        '#description' => $this->t('Field to use as the "alt" field for this bundle'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'alternate' => [
        'type' => 'varchar',
        'length' => 500,
        'not null' => FALSE,
      ],
      'description' => [
        'type' => 'varchar',
        'length' => 2000,
        'not null' => FALSE,
      ],
      'description_format' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ],
      'target_id' => [
        'type' => 'int',
        'length' => 10,
        'not null' => FALSE,
      ],
      'fid' => [
        'type' => 'int',
        'length' => 10,
        'not null' => TRUE,
      ],
    ];

    $indexes = [
      'target_id' => ['target_id'],
      'fid' => ['fid'],
    ];

    return [
      'columns' => $columns,
      'indexes' => $indexes,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['alternate'] = DataDefinition::create('string')
      ->setLabel(t('Alternate text'));

    $properties['description'] = DataDefinition::create('string')
      ->setLabel(t('Description'));

    $properties['description_format'] = DataDefinition::create('string')
      ->setLabel(t('Description format'));

    // This will be the entity_reference field itself.
    $properties['target_id'] = DataReferenceTargetDefinition::create('integer')
      ->setLabel(t('Photo reference'))
      ->setSetting('unsigned', TRUE);

    // This will be the entity_reference field itself.
    $properties['fid'] = DataReferenceTargetDefinition::create('integer')
      ->setLabel(t('File reference'))
      ->setSetting('unsigned', TRUE);

    // This is the definition of the reference target, it is needed but won't
    // appear as a field or a column in our table.
    $settings = $field_definition->getSettings();
    $target_type_info = \Drupal::entityTypeManager()
      ->getDefinition($settings['target_type']);

    $properties['referenced_entity'] = DataReferenceDefinition::create('entity')
      ->setLabel($target_type_info->getLabel())
      ->setDescription(t('The referenced entity'))
      ->setComputed(TRUE)
      ->setReadOnly(FALSE)
      ->setTargetDefinition(EntityDataDefinition::create($settings['target_type']))
      ->addConstraint('EntityType', $settings['target_type'])
      ->addConstraint('Bundle', [
        'photo',
      ]);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->get('target_id')->getValue()) && empty($this->get('fid')->getValue());
  }

}
