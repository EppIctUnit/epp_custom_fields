<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Provides a field type for EPP Publish field.
 *
 * @FieldType(
 *   id = "epp_publish",
 *   label = @Translation("EPP Publish"),
 *   description = @Translation("Stores the publication information"),
 *   category = @Translation("EPP"),
 *   default_formatter = "custom_fields_default_formatter",
 *   default_widget = "epp_publish_widget",
 * )
 */
class EPPPublish extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'general' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'intranet' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'internet' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'gsm' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'picture' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
    ];

    return [
      'columns' => $columns,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['general'] = DataDefinition::create('integer')
      ->setLabel(t('Can be published'))
      ->setRequired(TRUE);

    $properties['intranet'] = DataDefinition::create('integer')
      ->setLabel(t('Can be published on intranet'))
      ->setRequired(TRUE);

    $properties['internet'] = DataDefinition::create('integer')
      ->setLabel(t('Can be published on Internet'))
      ->setRequired(TRUE);

    $properties['gsm'] = DataDefinition::create('integer')
      ->setLabel(t('GSM can be published on Internet'))
      ->setRequired(TRUE);

    $properties['picture'] = DataDefinition::create('integer')
      ->setLabel(t('picture can be published on Internet'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->get('general')->getValue())
      && empty($this->get('intranet')->getValue())
      && empty($this->get('internet')->getValue())
      && empty($this->get('gsm')->getValue())
      && empty($this->get('picture')->getValue());
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    return [
      'general' => 0,
      'intranet' => 0,
      'internet' => 0,
      'gsm' => 0,
      'picture' => 0,
    ];
  }

}
