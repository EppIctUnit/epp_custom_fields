<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Provides a field type for EPP Socials field.
 *
 * @FieldType(
 *   id = "epp_socials",
 *   label = @Translation("EPP Socials"),
 *   description = @Translation("Store the social networks information"),
 *   category = @Translation("EPP"),
 *   default_formatter = "custom_fields_default_formatter",
 *   default_widget = "epp_socials_widget",
 * )
 */
class EPPSocials extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'twitter_handle' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ],
      'facebook' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ],
      'pinterest' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ],
      'instagram' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ],
      'youtube' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ],
      'linkedin' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ],
      'flicker' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ],
    ];

    return [
      'columns' => $columns,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['twitter_handle'] = DataDefinition::create('string')
      ->setLabel(t('Twitter handle'))
      ->setRequired(FALSE);

    $properties['facebook'] = DataDefinition::create('string')
      ->setLabel(t('Facebook URL'))
      ->setRequired(FALSE);

    $properties['pinterest'] = DataDefinition::create('string')
      ->setLabel(t('Pinterest URL'))
      ->setRequired(FALSE);

    $properties['instagram'] = DataDefinition::create('string')
      ->setLabel(t('Instagram URL'))
      ->setRequired(FALSE);

    $properties['youtube'] = DataDefinition::create('string')
      ->setLabel(t('YouTube URL'))
      ->setRequired(FALSE);

    $properties['linkedin'] = DataDefinition::create('string')
      ->setLabel(t('LinkedIn URL'))
      ->setRequired(FALSE);

    $properties['flicker'] = DataDefinition::create('string')
      ->setLabel(t('Flicker URL'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->get('twitter_handle')->getValue())
      && empty($this->get('facebook')->getValue())
      && empty($this->get('pinterest')->getValue())
      && empty($this->get('instagram')->getValue())
      && empty($this->get('youtube')->getValue())
      && empty($this->get('linkedin')->getValue())
      && empty($this->get('flicker')->getValue());
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    return [
      'twitter_handle' => 'Stub',
      'facebook' => 'Stub',
      'pinterest' => 'Stub',
      'instagram' => 'Stub',
      'youtube' => 'Stub',
      'linkedin' => 'Stub',
      'flicker' => 'Stub',
    ];
  }

}
