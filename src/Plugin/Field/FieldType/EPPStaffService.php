<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Entity\TypedData\EntityDataDefinition;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataReferenceDefinition;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;

/**
 * Provides a field type for EPP Staff Service field.
 *
 * @FieldType(
 *   id = "epp_staff_service",
 *   label = @Translation("EPP Staff Service"),
 *   description = @Translation("Field used to store the staff services"),
 *   default_formatter = "custom_fields_default_formatter",
 *   default_widget = "epp_staff_service_widget",
 *   category = @Translation("EPP"),
 * )
 */
class EPPStaffService extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'target_type' => 'node',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'handler' => 'default',
      'handler_settings' => [
        'target_bundles' => [
          'committee' => 'committee',
          'interparliamentary_delegation' => 'interparliamentary_delegation',
          'national_delegation' => 'national_delegation',
        ],
      ],
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    $element['target_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type of entity to reference'),
      '#options' => \Drupal::service('entity_type.repository')
        ->getEntityTypeLabels(TRUE),
      '#default_value' => $this->getSetting('target_type'),
      '#required' => TRUE,
      '#disabled' => $has_data,
      '#size' => 1,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::fieldSettingsForm($form, $form_state);
    $settings = $this->getSettings();

    // Get the reference target entity type from the storage settings.
    $target_type = $this->getSetting('target_type');

    // Get the bundle options.
    $options = [];
    $bundle_options = \Drupal::service('entity_type.bundle.info')
      ->getBundleInfo($target_type);
    foreach ($bundle_options as $key => $option) {
      $options[$key] = $option['label'];
    }

    // Make sure our default value is not erased on submit.
    $form['handler'] = [
      '#type' => 'hidden',
      '#value' => $settings['handler'] ?? 'default',
    ];

    $form['handler_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Entity reference settings'),
      '#attributes' => ['class' => ['entity_reference-settings']],
    ];

    $form['handler_settings']['target_bundles'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#title' => $this->t('Bundles'),
      '#default_value' => $settings['handler_settings']['target_bundles'],
      '#description' => $this->t('Bundles for the entity reference field'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'short_name' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ],
      'long_name' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ],
      'type' => [
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
      ],
      'service_code' => [
        'type' => 'int',
        'length' => 10,
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'body_code' => [
        'type' => 'varchar',
        'length' => 50,
        'not null' => FALSE,
      ],
      'target_id' => [
        'type' => 'int',
        'length' => 10,
        'unsigned' => TRUE,
        'not null' => FALSE,
      ],
      'important' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'responsible' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'precedence' => [
        'type' => 'int',
        'length' => 10,
        'unsigned' => FALSE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'internet_title' => [
        'type' => 'varchar',
        'length' => 1000,
        'not null' => FALSE,
      ],
      'internal_title' => [
        'type' => 'varchar',
        'length' => 1000,
        'not null' => TRUE,
      ],
    ];

    $indexes = [
      'services' => ['service_code'],
      'pbc' => ['target_id'],
    ];

    return [
      'columns' => $columns,
      'indexes' => $indexes,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Entity reference field definition.
    // This will be the entity_reference field itself.
    $properties['target_id'] = DataReferenceTargetDefinition::create('integer')
      ->setLabel(t('Body reference'))
      ->setSetting('unsigned', TRUE);

    // This is the definition of the reference target, it is needed but won't
    // appear as a field or a column in our table.
    $settings = $field_definition->getSettings();
    $target_type_info = \Drupal::entityTypeManager()
      ->getDefinition($settings['target_type']);

    $properties['referenced_entity'] = DataReferenceDefinition::create('entity')
      ->setLabel($target_type_info->getLabel())
      ->setDescription(t('The referenced entity'))
      ->setComputed(TRUE)
      ->setReadOnly(FALSE)
      ->setTargetDefinition(EntityDataDefinition::create($settings['target_type']))
      ->addConstraint('EntityType', $settings['target_type'])
      ->addConstraint('Bundle', [
        'committee',
        'interparliamentary_delegation',
        'national_delegation',
      ]);

    // Classic fields definition.
    $properties['short_name'] = DataDefinition::create('string')
      ->setLabel(t('Service short name'))
      ->addConstraint('NotNull')
      ->setRequired(TRUE);

    $properties['long_name'] = DataDefinition::create('string')
      ->setLabel(t('Service long name'))
      ->addConstraint('NotNull')
      ->setRequired(TRUE);

    $properties['type'] = DataDefinition::create('string')
      ->setLabel(t('Service type'))
      ->addConstraint('NotNull')
      ->setRequired(TRUE);

    $properties['service_code'] = DataDefinition::create('integer')
      ->setLabel(t('Service code'))
      ->addConstraint('NotNull')
      ->setRequired(TRUE);

    $properties['body_code'] = DataDefinition::create('string')
      ->setLabel(t('Body code'))
      ->setRequired(FALSE);

    $properties['important'] = DataDefinition::create('integer')
      ->setLabel(t('Importance for this service'))
      ->setRequired(TRUE);

    $properties['responsible'] = DataDefinition::create('integer')
      ->setLabel(t('Responsible for this service'))
      ->addConstraint('NotNull')
      ->setRequired(TRUE);

    $properties['precedence'] = DataDefinition::create('integer')
      ->setLabel(t('Precedence in service'))
      ->addConstraint('NotNull')
      ->setRequired(TRUE);

    $properties['internet_title'] = DataDefinition::create('string')
      ->setLabel(t('Title for Internet'))
      ->addConstraint('NotNull')
      ->setRequired(TRUE);

    $properties['internal_title'] = DataDefinition::create('string')
      ->setLabel(t('Title for EPP Group'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->get('service_code')->getValue());
  }

}
