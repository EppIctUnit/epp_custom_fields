<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Entity\TypedData\EntityDataDefinition;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataReferenceDefinition;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;

/**
 * Provides a EPP Vacancy field type.
 *
 * @FieldType(
 *   id = "epp_vacancy",
 *   label = @Translation("EPP Vacancy"),
 *   description = @Translation("Stores the MEP title information"),
 *   default_formatter = "custom_fields_default_formatter",
 *   default_widget = "epp_vacancy_widget",
 *   category = @Translation("EPP"),
 * )
 */
class EPPVacancy extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
        'grade_target_type' => 'taxonomy_term',
        'position_type_target_type' => 'taxonomy_term',
      ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
        'grade' => [
          'handler' => 'default',
          'handler_settings' => [
            'target_bundles' => [
              'grade' => 'grade',
            ],
          ],
        ],
        'position_type' => [
          'handler' => 'default',
          'handler_settings' => [
            'target_bundles' => [
              'position_type' => 'position_type',
            ],
          ],
        ],
      ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    $element['grade_target_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Grade: type of entity to reference'),
      '#options' => \Drupal::service('entity_type.repository')
        ->getEntityTypeLabels(TRUE),
      '#default_value' => $this->getSetting('grade_target_type'),
      '#required' => TRUE,
      '#disabled' => $has_data,
      '#size' => 1,
    ];

    $element['position_type_target_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Position type: type of entity to reference'),
      '#options' => \Drupal::service('entity_type.repository')
        ->getEntityTypeLabels(TRUE),
      '#default_value' => $this->getSetting('position_type_target_type'),
      '#required' => TRUE,
      '#disabled' => $has_data,
      '#size' => 1,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::fieldSettingsForm($form, $form_state);
    $settings = $this->getSettings();
    $fields = [
      'grade' => $this->t('Grade'),
      'position_type' => $this->t('Position type'),
    ];

    foreach ($fields as $machine_name => $field_label) {
      // Get the reference target entity type from the storage settings.
      $target_type = $settings[$machine_name . '_target_type'];

      // Get the bundle options.
      $options = [];
      $bundle_options = \Drupal::service('entity_type.bundle.info')
        ->getBundleInfo($target_type);
      foreach ($bundle_options as $key => $option) {
        $options[$key] = $option['label'];
      }

      // Make sure our default value is not erased on submit.
      $form[$machine_name]['handler'] = [
        '#type' => 'hidden',
        '#value' => $settings[$machine_name]['handler'] ?? 'default',
      ];

      $form[$machine_name]['handler_settings'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('@label entity reference settings', ['@label' => $field_label]),
        '#attributes' => ['class' => ['entity_reference-settings']],
      ];

      $form[$machine_name]['handler_settings']['target_bundles'] = [
        '#type' => 'checkboxes',
        '#options' => $options,
        '#title' => $this->t('Bundles'),
        '#default_value' => $settings[$machine_name]['handler_settings']['target_bundles'],
        '#description' => $this->t('@label entity reference bundles', ['@label' => $field_label]),
        '#required' => TRUE,
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'reference' => [
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
      ],
      'grade_target_id' => [
        'type' => 'int',
        'length' => 10,
        'not null' => TRUE,
        'unsigned' => TRUE,
      ],
      'description' => [
        'type' => 'varchar',
        'length' => 500,
        'not null' => TRUE,
      ],
      'position_type_target_id' => [
        'type' => 'int',
        'length' => 10,
        'not null' => TRUE,
        'unsigned' => TRUE,
      ],
      'deadline' => [
        'type' => 'int',
        'length' => 10,
        'not null' => TRUE,
        'unsigned' => TRUE,
      ],
      'competition_date' => [
        'type' => 'varchar',
        'length' => 100,
        'not null' => FALSE,
        'default' => NULL,
      ],
      'result_date' => [
        'type' => 'varchar',
        'length' => 100,
        'not null' => FALSE,
        'default' => NULL,
      ],
      'minimum_salary' => [
        'type' => 'varchar',
        'length' => 500,
        'not null' => FALSE,
        'default' => NULL,
      ],
      'apply_link' => [
        'type' => 'varchar',
        'length'        => 500,
        'not null'      => TRUE,
      ],
    ];

    $indexes = [
      'reference' => ['reference'],
    ];

    return [
      'columns' => $columns,
      'indexes' => $indexes,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Entity reference fields definition.
    $fields = [
      'grade' => t('Grade'),
      'position_type' => t('Position type'),
    ];

    foreach ($fields as $machine_name => $field_label) {
      // This will be the entity_reference field itself.
      $properties[$machine_name . '_target_id'] = DataReferenceTargetDefinition::create('integer')
        ->setLabel($field_label)
        ->setSetting('unsigned', TRUE);

      // This is the definition of the reference target, it is needed but won't
      // appear as a field or a column in our table.
      $settings = $field_definition->getSettings();
      $target_type_info = \Drupal::entityTypeManager()
        ->getDefinition($settings[$machine_name . '_target_type']);

      $properties[$machine_name . '_referenced_entity'] = DataReferenceDefinition::create('entity')
        ->setLabel($target_type_info->getLabel())
        ->setDescription(t('@field referenced entity', ['@field' => $field_label]))
        ->setComputed(TRUE)
        ->setReadOnly(FALSE)
        ->setTargetDefinition(EntityDataDefinition::create($settings[$machine_name . '_target_type']))
        ->addConstraint('EntityType', $settings[$machine_name . '_target_type'])
        ->addConstraint('Bundle', [$machine_name]);
    }

    // Classic fields definition.
    $properties['reference'] = DataDefinition::create('string')
      ->setLabel(t('Vacancy reference'))
      ->addConstraint('NotNull')
      ->setRequired(TRUE);

    $properties['description'] = DataDefinition::create('string')
      ->setLabel(t('Job title'))
      ->addConstraint('NotNull')
      ->setRequired(TRUE);

    $properties['deadline'] = DataDefinition::create('timestamp')
      ->setLabel(t('Vacancy deadline'))
      ->addConstraint('NotNull')
      ->setRequired(TRUE);

    $properties['competition_date'] = DataDefinition::create('string')
      ->setLabel(t('Vacancy competition date'))
      ->setRequired(FALSE);

    $properties['result_date'] = DataDefinition::create('string')
      ->setLabel(t('Vacancy result date'))
      ->setRequired(FALSE);

    $properties['minimum_salary'] = DataDefinition::create('string')
      ->setLabel(t('Minimum Salary'))
      ->setRequired(FALSE);

    $properties['apply_link'] = DataDefinition::create('string')
      ->setLabel(t('Link to application form'))
      ->addConstraint('NotNull')
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    // Since all the columns are 'not null', require all the values.
    if (empty($this->get('reference')->getValue())
      || empty($this->get('description')->getValue())
      || empty($this->get('grade_target_id')->getValue())
      || empty($this->get('position_type_target_id')->getValue())
      || empty($this->get('deadline')->getValue())
      || empty($this->get('apply_link')->getValue())) {
      return TRUE;
    }
    return FALSE;
  }

}
