<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\Form\FormStateInterface;

/**
 * Represents a configurable entity epp picture field.
 */
class EppPictureFieldItemList extends FieldItemList {

  /**
   * {@inheritdoc}
   */
  public function defaultValuesForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * Load files available in the field current value.
   *
   * @return array
   *   The loaded files.
   */
  protected function getFiles() {
    $entityTypeManager = \Drupal::entityTypeManager();

    $files = [];
    foreach ($this->list as $element) {
      $files[] = $entityTypeManager->getStorage('file')
                                   ->load($element->getValue()['fid']);
    }

    return $files;
  }

  /**
   * {@inheritdoc}
   */
  public function postSave($update) {
    $fileUsage = \Drupal::service('file.usage');

    $entity = $this->getEntity();
    $files  = $this->getFiles();

    if (!$update) {
      // Add a new usage for newly uploaded files.
      foreach ($files as $file) {
        if (!empty($file)) {
          $fileUsage->add($file, 'file', $entity->getEntityTypeId(), $entity->id());
        }
      }
    }
    else {
      // Get current target file entities and file IDs.
      $ids = [];

      /** @var \Drupal\file\FileInterface $file */
      foreach ($files as $file) {
        if (!empty($file)) {
          $ids[] = $file->id();
        }
      }

      // On new revisions, all files are considered to be a new usage and no
      // deletion of previous file usages are necessary.
      if (!empty($entity->original) && $entity->getRevisionId() != $entity->original->getRevisionId()) {
        foreach ($files as $file) {
          if (!empty($file)) {
            $fileUsage->add($file, 'file', $entity->getEntityTypeId(), $entity->id());
          }
        }
        return;
      }

      // Get the file IDs attached to the field before this update.
      $field_name   = $this->getFieldDefinition()->getName();
      $original_ids = [];
      $langcode     = $this->getLangcode();
      $original     = $entity->original;
      if ($original->hasTranslation($langcode)) {
        $original_items = $original->getTranslation($langcode)->{$field_name};
        foreach ($original_items as $item) {
          $original_ids[] = $item->fid;
        }
      }

      // Decrement file usage by 1 for files that were removed from the field.
      $removed_ids   = array_filter(array_diff($original_ids, $ids));
      $removed_files = \Drupal::entityTypeManager()
                              ->getStorage('file')
                              ->loadMultiple($removed_ids);
      foreach ($removed_files as $file) {
        if (!empty($file)) {
          $fileUsage->delete($file, 'file', $entity->getEntityTypeId(), $entity->id());
        }
      }

      // Add new usage entries for newly added files.
      foreach ($files as $file) {
        if (!empty($file) && !in_array($file->id(), $original_ids)) {
          $fileUsage->add($file, 'file', $entity->getEntityTypeId(), $entity->id());
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    parent::delete();
    $entity = $this->getEntity();

    $fileUsage = \Drupal::service('file.usage');

    $files = $this->getFiles();

    // If a translation is deleted only decrement the file usage by one. If the
    // default translation is deleted remove all file usages within this entity.
    $count = $entity->isDefaultTranslation() ? 0 : 1;
    foreach ($files as $file) {
      $fileUsage->delete($file, 'file', $entity->getEntityTypeId(), $entity->id(), $count);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function deleteRevision() {
    parent::deleteRevision();
    $entity = $this->getEntity();

    $fileUsage = \Drupal::service('file.usage');

    $files = $this->getFiles();

    // Decrement the file usage by 1.
    foreach ($files as $file) {
      $fileUsage->delete($file, 'file', $entity->getEntityTypeId(), $entity->id());
    }
  }

}
