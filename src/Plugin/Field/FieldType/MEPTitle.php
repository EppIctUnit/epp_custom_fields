<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Entity\TypedData\EntityDataDefinition;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\DataReferenceDefinition;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;

/**
 * Provides a MEP Title field type.
 *
 * @FieldType(
 *   id = "mep_title",
 *   label = @Translation("MEP Title"),
 *   description = @Translation("Stores the MEP title information"),
 *   default_formatter = "custom_fields_default_formatter",
 *   default_widget = "mep_title_widget",
 *   category = @Translation("EPP"),
 * )
 */
class MEPTitle extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'target_type' => 'node',
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'handler' => 'default',
      'handler_settings' => [
        'target_bundles' => [
          'committee' => 'committee',
          'interparliamentary_delegation' => 'interparliamentary_delegation',
          'national_delegation' => 'national_delegation',
          'party' => 'party',
          'working_group' => 'working_group',
        ],
      ],
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);

    $element['target_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type of entity to reference'),
      '#options' => \Drupal::service('entity_type.repository')
        ->getEntityTypeLabels(TRUE),
      '#default_value' => $this->getSetting('target_type'),
      '#required' => TRUE,
      '#disabled' => $has_data,
      '#size' => 1,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::fieldSettingsForm($form, $form_state);
    $settings = $this->getSettings();

    // Get the reference target entity type from the storage settings.
    $target_type = $this->getSetting('target_type');

    // Get the bundle options.
    $options = [];
    $bundle_options = \Drupal::service('entity_type.bundle.info')
      ->getBundleInfo($target_type);
    foreach ($bundle_options as $key => $option) {
      $options[$key] = $option['label'];
    }

    // Make sure our default value is not erased on submit.
    $form['handler'] = [
      '#type' => 'hidden',
      '#value' => $settings['handler'] ?? 'default',
    ];

    $form['handler_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Entity reference settings'),
    ];
    $form['handler_settings']['target_bundles'] = [
      '#type' => 'checkboxes',
      '#options' => $options,
      '#title' => $this->t('Bundles'),
      '#default_value' => $settings['handler_settings']['target_bundles'],
      '#description' => $this->t('Bundles for the entity reference field'),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'title' => [
        'type' => 'varchar',
        'length' => 2048,
        'not null' => TRUE,
      ],
      'function_name' => [
        'type' => 'varchar',
        'length' => 100,
        'not null' => TRUE,
      ],
      'function_code' => [
        'type' => 'varchar',
        'length' => 50,
        'not null' => TRUE,
      ],
      'function_weight' => [
        'type' => 'int',
        'length' => 10,
        'unsigned' => FALSE,
        'not null' => TRUE,
        'default' => 0,
      ],
      'body_name' => [
        'type' => 'varchar',
        'length' => 2048,
        'not null' => FALSE,
      ],
      'body_long_name' => [
        'type' => 'varchar',
        'length' => 2048,
        'not null' => FALSE,
      ],
      'body_code' => [
        'type' => 'varchar',
        'length' => 50,
        'not null' => FALSE,
      ],
      'body_weight' => [
        'type' => 'int',
        'length' => 10,
        'unsigned' => FALSE,
        'not null' => FALSE,
      ],
      'bodytype_code' => [
        'type' => 'varchar',
        'length' => 50,
        'not null' => FALSE,
      ],
      'bodytype_weight' => [
        'type' => 'int',
        'length' => 10,
        'unsigned' => FALSE,
        'not null' => FALSE,
      ],
      'target_id' => [
        'type' => 'int',
        'length' => 10,
        'unsigned' => TRUE,
        'not null' => FALSE,
      ],
    ];

    $indexes = [
      'fctcode' => ['function_code'],
      'bodycode' => ['body_code'],
      'bodytypecode' => ['bodytype_code'],
    ];

    return [
      'columns' => $columns,
      'indexes' => $indexes,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    // Entity reference field definition.
    // This will be the entity_reference field itself.
    $properties['target_id'] = DataReferenceTargetDefinition::create('integer')
      ->setLabel(t('Body node reference'))
      ->setSetting('unsigned', TRUE);

    // This is the definition of the reference target, it is needed but won't
    // appear as a field or a column in our table.
    $settings = $field_definition->getSettings();
    $target_type_info = \Drupal::entityTypeManager()
      ->getDefinition($settings['target_type']);

    $properties['referenced_entity'] = DataReferenceDefinition::create('entity')
      ->setLabel($target_type_info->getLabel())
      ->setDescription(t('The referenced entity'))
      ->setComputed(TRUE)
      ->setReadOnly(FALSE)
      ->setTargetDefinition(EntityDataDefinition::create($settings['target_type']))
      ->addConstraint('EntityType', $settings['target_type'])
      ->addConstraint('Bundle', [
        'committee',
        'interparliamentary_delegation',
        'national_delegation',
        'party',
        'working_group',
      ]);

    // Classic fields definition.
    $properties['title'] = DataDefinition::create('string')
      ->setLabel(t('Title'))
      ->addConstraint('NotNull')
      ->setRequired(TRUE);

    $properties['function_name'] = DataDefinition::create('string')
      ->setLabel(t('Function name'))
      ->addConstraint('NotNull')
      ->setRequired(TRUE);

    $properties['function_code'] = DataDefinition::create('string')
      ->setLabel(t('Function code'))
      ->addConstraint('NotNull')
      ->setRequired(TRUE);

    $properties['function_weight'] = DataDefinition::create('integer')
      ->setLabel(t('Function precedence'))
      ->addConstraint('NotNull')
      ->setRequired(TRUE);

    $properties['body_name'] = DataDefinition::create('string')
      ->setLabel(t('Body short name'))
      ->setRequired(FALSE);

    $properties['body_long_name'] = DataDefinition::create('string')
      ->setLabel(t('Body long name'))
      ->setRequired(FALSE);

    $properties['body_code'] = DataDefinition::create('string')
      ->setLabel(t('Body code'))
      ->setRequired(FALSE);

    $properties['body_weight'] = DataDefinition::create('integer')
      ->setLabel(t('Body precedence'))
      ->setRequired(FALSE);

    $properties['bodytype_code'] = DataDefinition::create('string')
      ->setLabel(t('Body type code'))
      ->setRequired(FALSE);

    $properties['bodytype_weight'] = DataDefinition::create('integer')
      ->setLabel(t('Body type precedence'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->get('title')->getValue())
      || empty($this->get('function_name')->getValue())
      || empty($this->get('function_code')->getValue())
      || (empty($this->get('function_weight')->getValue()) && ($this->get('function_weight')->getValue() != 0));
  }

}
