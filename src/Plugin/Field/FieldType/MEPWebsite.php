<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Provides a field type for MEP Website field.
 *
 * @FieldType(
 *   id = "mep_website",
 *   label = @Translation("MEP Website"),
 *   description = @Translation("Stores the MEP website information"),
 *   category = @Translation("EPP"),
 *   default_formatter = "custom_fields_default_formatter",
 *   default_widget = "mep_website_widget",
 * )
 */
class MEPWebsite extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $columns = [
      'type' => [
        'type' => 'varchar',
        'length' => 50,
        'not null' => FALSE,
      ],
      'url' => [
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ],
    ];

    $indexes = [
      'website_types' => ['type'],
    ];

    return [
      'columns' => $columns,
      'indexes' => $indexes,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['type'] = DataDefinition::create('string')
      ->setLabel(t('Type'));

    $properties['url'] = DataDefinition::create('string')
      ->setLabel(t('URL'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return empty($this->get('type')->getValue())
      && empty($this->get('url')->getValue());
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    return [
      'type' => 'Stub',
      'url' => 'Stub',
    ];
  }

}
