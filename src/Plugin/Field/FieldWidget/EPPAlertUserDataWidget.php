<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Render\Element\Number;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'epp_alert_user_data' widget.
 *
 * @FieldWidget(
 *   id = "epp_alert_user_data_widget",
 *   label = @Translation("EPP Alert User Data"),
 *   field_types = {
 *     "epp_alert_user_data"
 *   }
 * )
 */
class EPPAlertUserDataWidget extends WidgetBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * EPPMepInvolvedWidget constructor extends WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityTypeManagerInterface $entity_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality == 1) {
      $element['#type'] = 'fieldset';
      $element['#collapsible'] = TRUE;
      $element['#collapsed'] = FALSE;
    }

    $settings = $this->getFieldSettings();
    $target_type = $settings['user_target_type'];
    $target_id_setting = 'target_id';
    $default = isset($items[$delta]) ? $items[$delta]->$target_id_setting : NULL;
    $handler_settings = $settings['user']['handler_settings'];
    $machine_name = 'user';

    if (!empty($default)) {
      $default = $this->entityManager
        ->getStorage($target_type)
        ->load($default);
    }

    $element['target_id'] = [
      '#type' => 'entity_autocomplete',
      '#title' => 'User reference',
      '#target_type' => $target_type,
      '#selection_handler' => $settings[$machine_name]['handler'] ?? 'default',
      '#selection_settings' => $handler_settings,
      '#default_value' => $default ?? NULL,
    ];

    $element['target_id']['#access'] = FALSE;
    $element['target_id']['#description'] = $this->t('This field is a technical field, it is automatically filled');

    $element['is_open'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is still opened'),
      '#maxlength' => 10,
      '#default_value' => !empty($items[$delta]->is_open) ? 1 : 0,
      '#element_validate' => [
        [Number::class, 'validateNumber'],
      ],
    ];

    $element['discarded'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Has been discarded'),
      '#maxlength' => 10,
      '#default_value' => !empty($items[$delta]->discarded) ? 1 : 0,
      '#element_validate' => [
        [Number::class, 'validateNumber'],
      ],
    ];

    $element['executed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Has been executed'),
      '#maxlength' => 10,
      '#default_value' => !empty($items[$delta]->executed) ? 1 : 0,
      '#element_validate' => [
        [Number::class, 'validateNumber'],
      ],
    ];

    $element['execution_time'] = [
      '#type' => 'number',
      '#title' => $this->t('Execution time'),
      '#maxlength' => 10,
      '#default_value' => !empty($items[$delta]->execution_time) ? $items[$delta]->execution_time : 0,
      '#element_validate' => [
        [Number::class, 'validateNumber'],
      ],
    ];

    $element['action_result'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Results'),
      '#maxlength' => 255,
      '#default_value' => $items[$delta]->action_result ?? NULL,
    ];

    $element['displayed_number'] = [
      '#type' => 'number',
      '#title' => $this->t('How many time displayed'),
      '#maxlength' => 10,
      '#default_value' => !empty($items[$delta]->displayed_number) ? $items[$delta]->displayed_number : 0,
      '#element_validate' => [
        [Number::class, 'validateNumber'],
      ],
    ];

    // As the widget has multiple fields, we need to use a custom
    // validation method to provide a better understanding error message.
    $element['#element_validate'][] = [
      static::class,
      'validateElement',
    ];

    return $element;
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if (!empty($element['#required']) && empty($element['target_id']['#value'])) {
      $form_state->setError($element['target_id'], t("The user reference of the @name field is required.", ['@name' => $element['#title']]));
    }
  }

}
