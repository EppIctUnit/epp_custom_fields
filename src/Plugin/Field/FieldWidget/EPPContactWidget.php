<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'epp_contact' widget.
 *
 * @FieldWidget(
 *   id = "epp_contact_widget",
 *   label = @Translation("EPP Contact widget"),
 *   field_types = {
 *     "epp_contact"
 *   }
 * )
 */
class EPPContactWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality == 1) {
      $element['#type'] = 'fieldset';
      $element['#collapsible'] = TRUE;
      $element['#collapsed'] = FALSE;
    }

    $element['location'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Location'),
      '#maxlength' => 512,
      '#default_value' => $items[$delta]->location ?? NULL,
    ];

    $element['office'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Office'),
      '#maxlength' => 50,
      '#default_value' => $items[$delta]->office ?? NULL,
    ];

    $element['address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Address'),
      '#maxlength' => 512,
      '#default_value' => $items[$delta]->address ?? NULL,
    ];

    $element['phone'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Phone'),
      '#maxlength' => 30,
      '#default_value' => $items[$delta]->phone ?? NULL,
    ];

    $element['mobile'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mobile'),
      '#maxlength' => 30,
      '#default_value' => $items[$delta]->mobile ?? NULL,
    ];

    $element['fax'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Fax'),
      '#maxlength' => 30,
      '#default_value' => $items[$delta]->fax ?? NULL,
    ];

    // As the widget has multiple fields, we need to use a custom
    // validation method to provide a better understanding error message.
    $element['#element_validate'][] = [
      static::class,
      'validateElement',
    ];

    return $element;
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if (!empty($element['#required'])
      && (empty($element['location']['#value'])
      && empty($element['office']['#value'])
      && empty($element['address']['#value'])
      && empty($element['phone']['#value'])
      && empty($element['mobile']['#value'])
      && empty($element['fax']['#value']))
    ) {
      $form_state->setError($element, t('At least one field of the @name field is required.', ['@name' => $element['#title']]));
    }
  }

}
