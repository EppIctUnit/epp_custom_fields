<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'epp_contact' widget.
 *
 * @FieldWidget(
 *   id = "epp_csp_widget",
 *   label = @Translation("EPP Csp widget"),
 *   field_types = {
 *     "epp_csp"
 *   }
 * )
 */
class EPPCspWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality == 1) {
      $element['#type'] = 'fieldset';
      $element['#collapsible'] = TRUE;
      $element['#collapsed'] = FALSE;
    }

    $element['type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Location'),
      '#default_value' => $items[$delta]->type ?? NULL,
    ];

    $options = $this->getCspType();
    $element['type'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => $this->t('CSP TYPE'),
      '#default_value' => $items[$delta]->type ?? NULL,
    ];
    $element['api'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API URL'),
      '#default_value' => $items[$delta]->api ?? NULL,
    ];
    // As the widget has multiple fields, we need to use a custom
    // validation method to provide a better understanding error message.
    $element['#element_validate'][] = [
      static::class,
      'validateElement',
    ];

    return $element;
  }

  /**
   * Get list ok csp type
   */
  public function getCspType () {
    $types = [
      0 => $this->t('--- CSP Type ---'),
      'default-src' => 'default src',
      'font-src' => 'font src',
      'frame-src' => 'frame src',
      'img-src' => 'img src',
      'media-src' => 'media src',
      'object-src' => 'object src',
      'script-src' => 'script src',
      'connect-src' => 'connect src',
      'style-src' => 'style src',
      'form-src' => 'form src',
      'frame-src' => 'frame src',
    ];
    return $types;
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if (!empty($element['#required'])
      && (empty($element['type']['#value'])
      || empty($element['api']['#value']))
    ) {
      $form_state->setError($element, t('At least one field of the @name field is required.', ['@name' => $element['#title']]));
    }
  }

}
