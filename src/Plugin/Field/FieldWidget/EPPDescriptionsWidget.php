<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'epp_descriptions' widget.
 *
 * @FieldWidget(
 *   id = "epp_descriptions_widget",
 *   label = @Translation("EPP Descriptions widget"),
 *   field_types = {
 *     "epp_descriptions"
 *   }
 * )
 */
class EPPDescriptionsWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality == 1) {
      $element['#type'] = 'fieldset';
      $element['#collapsible'] = TRUE;
      $element['#collapsed'] = FALSE;
    }

    $element['short'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Short description'),
      '#default_value' => $items[$delta]->short ?? NULL,
    ];

    $element['long'] = [
      '#type' => 'textarea',
      '#maxlength' => 500,
      '#description' => $this->t('500 character limit on text field'),
      '#title' => $this->t('Long description'),
      '#default_value' => $items[$delta]->long ?? NULL,
    ];

    $element['mobile'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description for mobile'),
      '#default_value' => $items[$delta]->mobile ?? NULL,
    ];

    // As the widget has multiple fields, we need to use a custom
    // validation method to provide a better understanding error message.
    $element['#element_validate'][] = [
      static::class,
      'validateElement',
    ];

    return $element;
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if (!empty($element['#required'])
      && (empty($element['short']['#value'])
      && empty($element['long']['#value'])
      && empty($element['mobile']['#value']))
    ) {
      $form_state->setError($element, t('At least one field among Short description, Long description and Description for mobile of the @name field is required.', ['@name' => $element['#title']]));
    }
  }

}
