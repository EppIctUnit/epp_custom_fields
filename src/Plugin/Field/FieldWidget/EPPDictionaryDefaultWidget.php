<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Security\TrustedCallbackInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'epp_dictionary' default widget.
 *
 * @FieldWidget(
 *   id = "epp_dictionary_default_widget",
 *   label = @Translation("EPP dictionary widget - Default"),
 *   field_types = {
 *     "epp_dictionary"
 *   }
 * )
 */
class EPPDictionaryDefaultWidget extends WidgetBase implements TrustedCallbackInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The cache service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityTypeManagerInterface $entityTypeManager, Connection $database, CacheBackendInterface $cache) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityTypeManager = $entityTypeManager;
    $this->database          = $database;
    $this->cache             = $cache;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('cache.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return [
      'selectPreRender',
    ];
  }

  /**
   * Pre render callback for select form elements.
   *
   * @param   array  $element
   *   The form element.
   *
   * @return array
   *   The form element.
   */
  public static function selectPreRender(array $element): array {
    // Attach the Choices.js library on selects that require it.
    if (isset($element['#choices_js'])) {
      $element['#attributes']['class'][] = 'choices-enabled';
      $element['#attached']['library'][] = 'epp_custom_fields/epp.choices.js';
    }
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function form(FieldItemListInterface $items, array &$form, FormStateInterface $form_state, $get_delta = NULL) {
    $parent = parent::form($items, $form, $form_state, $get_delta);

    // Hide all the elements of a multiple field.
    if ($parent['widget']['#cardinality_multiple'] === TRUE) {
      unset($parent['widget']['#prefix'], $parent['widget']['#suffix'], $parent['widget']['add_more']);
      $children = Element::children($parent['widget']);
      foreach ($children as $child) {
        unset($parent['widget'][$child]['_weight']);
      }
      $parent['widget']['#cardinality_multiple'] = FALSE;
      $parent['widget']['#cardinality']          = 1;
    }

    return $parent;
  }

  /**
   * {@inheritdoc}
   */
  protected function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {
    $elements = $default_value = $options = $termList = [];
    // Required information.
    $elements += [
      '#cardinality_multiple' => $this->fieldDefinition->getFieldStorageDefinition()
                                                       ->isMultiple(),
    ];

    // Build the term options.
    $settings = $this->getFieldSettings();
    if (isset($settings['handler_settings']['target_bundles'])) {
      $vocabularies = array_filter($settings['handler_settings']['target_bundles']);
      foreach ($vocabularies as $vocabulary) {
        $opt = $this->getTaxonomyOptions($vocabulary, $settings, $form, $form_state);
        if (!empty($opt['termsListByVocabulary'])) {
          $termList += $opt['termsListByVocabulary'];
        }
        if (isset($opt['optionsList'])) {
          if (count($vocabularies) > 1) {
            $option = [];
            foreach ($opt['optionsList'] as $key => $val) {
              $option["$key [$vocabulary]"] = $val;
            }
          }
          else {
            $option = $opt['optionsList'];
          }
        }
        else {
          $option = $opt;
        }
        $options += $option;
      }
    }
    $allow = $this->getSetting('structured_conditional_allow_parent') ?? FALSE;
    if (!$allow) {
      asort($options);
    }

    // Determine the number of fields to create (copied from parent method).
    $field_name  = $this->fieldDefinition->getName();
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()
                                         ->getCardinality();
    $parents     = $form['#parents'];

    switch ($cardinality) {
      case FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED:
        $field_state = static::getWidgetState($parents, $field_name, $form_state);
        $max         = $field_state['items_count'];
        break;

      default:
        $max = $cardinality - 1;
        break;
    }

    // Create a stid field for each item & build the default_value list for the
    // placeholder field.
    for ($delta = 0; $delta <= $max; $delta++) {
      // This field is hidden and filled on submit.
      $elements[$delta]['stid'] = [
        '#type'          => 'textfield',
        '#title'         => $this->t('Dictionary name reference'),
        '#size'          => 60,
        '#maxlength'     => 15,
        '#default_value' => $items->getValue()[$delta]['stid'] ?? NULL,
        '#access'        => FALSE,
      ];

      $default_value[] = $items->getValue()[$delta]['target_id'] ?? NULL;
    }

    // Wrap everything in a fieldset for clarity.
    $elements['#type']          = 'fieldset';
    $elements['#collapsible']   = TRUE;
    $elements['#collapsed']     = FALSE;
    $elements['#title']         = $this->fieldDefinition->getLabel();
    $elements['#title_display'] = 'before';
    $elements['#required']      = $this->fieldDefinition->isRequired();

    // As the widget has multiple fields, we need to use a custom
    // validation method to provide a better understanding error message.
    $elements['#element_validate'][] = [
      static::class,
      'validateElement',
    ];

    // Default select field with common values.
    $select = [
      '#type'         => 'select',
      //'#title' => $this->t('Term reference'),
      '#options'      => $options,
      '#empty_option' => $this->t('Select'),
      '#choices_js'   => TRUE,
      '#properties'   => ['optgroups' => TRUE],
      '#required'     => $this->fieldDefinition->isRequired(),
      '#terms_list'   => $termList ?? NULL,
    ];
    // Display the target_id field for single value fields.
    if ($cardinality === 1) {
      $elements[0]['target_id']                   = $select;
      $elements[0]['target_id']['#default_value'] = $items->getValue()[0]['target_id'] ?? NULL;
    }
    // Otherwise display a placeholder containing all the values in one field.
    else {
      $elements[0]['placeholder']                   = $select;
      $elements[0]['placeholder']['#default_value'] = $default_value;
      $elements[0]['placeholder']['#multiple']      = TRUE;
    }
    $elements = $this->alterElement($elements, $items);
    return $elements;
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param   array                                 $element
   *   The form element.
   * @param   \Drupal\Core\Form\FormStateInterface  $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if (!empty($element['#required']) && empty($element[0]['placeholder']['#value']) && empty($element[0]['target_id']['#value'])) {
      $form_state->setError($element, t('@name field is required.', ['@name' => $element['#title']]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    // Do nothing but this implementation is required.
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');
    $values       = parent::massageFormValues($values, $form, $form_state);
    // If we have a placeholder, then it's a multiple value field and we need to
    // store all each value separately.
    if (array_key_exists('placeholder', $values[0])) {
      $massaged_values = [];
      $placeholder     = $values[0]['placeholder'];
      $delta           = 0;
      foreach ($placeholder as $value) {
        if (!empty($value)) {
          $term = $term_storage->load($value);
          // Mimic the native structure to ensure that it will be properly saved.
          $massaged_values[$delta] = [
            'stid'            => $term->label() ?? '',
            'target_id'       => $value,
            '_original_delta' => $delta,
          ];
          $delta++;
        }
      }
      $values = $massaged_values;
    }
    // Otherwise it's a single value field, just fill the hidden field.
    elseif (!empty($values[0]['target_id'])) {
      $term              = $term_storage->load($values[0]['target_id']);
      $values[0]['stid'] = $term->label() ?? '';
    }

    return $values;
  }

  /**
   * Get an options list from taxonomy vocabularies.
   *
   * @param   string  $vocabulary
   *   The vocabulary.
   * @param   array   $settings
   *   The field settings.
   *
   * @return array
   *   Options with optionsets.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTaxonomyOptions($vocabulary, array $settings, array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\taxonomy\TermStorageInterface $term_storage */
    $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');
    $options      = [];
    $terms        = $term_storage->loadTree($vocabulary, 0, NULL, TRUE);

    // Filter countries options if the access delegation option is checked.
    $accessDelegation = $vocabulary == 'countries' && !empty($settings['handler_settings']['access_delegation_container']['access_delegation']);

    // Create the options.
    foreach ($terms as $term) {
      if (isset($term->field_epp_formatted_value)) {
        $formatted_value = $term->field_epp_formatted_value->getValue();

        if (!$accessDelegation || !empty($term->field_is_delegation->value)) {
          $options[$term->id()] = $formatted_value[0]['safe_value'] ?? ($formatted_value[0]['value'] ?? $term->label());
        }
      }
      else {
        $options[$term->id()] = $term->label();
      }
    }

    // Special case for bodies: unset the term if the related node committee
    // is marked as former.
    if ($vocabulary === 'bodies') {
      $options = $this->filterBodiesOptions($options);
    }

    return $options;
  }

  /**
   * Alter element list.
   *
   * @param   array  $element
   *   The form element.
   *
   * @return mixed
   */
  public function alterElement(array $element, FieldItemListInterface $items) {
    return $element;
  }

  /**
   * Filter out terms related to a committee node marked as former.
   *
   * @param   array  $options
   *   The select options.
   *
   * @return array
   *   The filtered select options.
   */
  public function filterBodiesOptions(array $options): array {
    // Load the cached values, if existing.
    $is_cached = $this->cache->get('epp_custom_fields::bodies_to_unset');

    if ($is_cached) {
      $to_unset = $is_cached->data;
    }
    else {
      // Get all the committee nodes that are 'former'.
      $query = $this->database->select('node__field_is_former', 'former');
      $query->condition('former.bundle', 'committee', '=');
      $query->condition('former.field_is_former_value', 1, '=');
      // Get the term name from field_body_s_metadata and map with the terms
      // data table to get the term id.
      $query->leftJoin('node__field_body_s_metadata', 'metadata', 'metadata.entity_id = former.entity_id');
      $query->leftJoin('taxonomy_term_field_data', 'term_data', 'term_data.name = metadata.field_body_s_metadata_body_code');
      // Filter by language to avoid duplicates and empty values (some body
      // codes don't have a corresponding term at all).
      $query->condition('term_data.langcode', 'en', '=');
      $query->fields('term_data', ['tid']);
      $to_unset = $query->execute()->fetchCol(0);

      // Cache the result of the query until bodies terms or committees nodes
      // are modified.
      $tags = ['node_list:committee', 'taxonomy_term_list:bodies'];
      $this->cache->set('epp_custom_fields::bodies_to_unset', $to_unset, CacheBackendInterface::CACHE_PERMANENT, $tags);
    }

    // Unset the IDs from the options.
    foreach ($to_unset as $id) {
      if (array_key_exists($id, $options)) {
        unset($options[$id]);
      }
    }

    return $options;
  }

}
