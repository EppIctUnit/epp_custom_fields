<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'epp_dictionary' structured widget.
 *
 * @FieldWidget(
 *   id = "epp_dictionary_structured_cond_widget",
 *   label = @Translation("EPP dictionary widget - Structured conditional"),
 *   field_types = {
 *     "epp_dictionary"
 *   }
 * )
 */
class EPPDictionaryStructuredConditionalWidget extends EPPDictionaryStructuredWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'structured_conditional_condition' => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $element['structured_conditional_condition'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Condition'),
      '#default_value' => $this->getSetting('structured_conditional_condition') ?? '',
      '#description' => $this->t("Set name|value|vocabulary each on a separate line."),
    ];
    return $element;
  }

  /**
   * Get a hierarchical options list from taxonomy vocabularies.
   *
   * @param string $vocabulary
   *   The vocabulary.
   * @param array $settings
   *   The field settings.
   *
   * @return array
   *   Options with optionsets.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTaxonomyOptions($vocabulary, array $settings, array &$form, FormStateInterface $form_state) {
    return parent::getTaxonomyOptions($vocabulary, $settings, $form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function alterElement(array $element, FieldItemListInterface $items) {
    $element = parent::alterElement($element, $items);
    $condition = $this->getSetting('structured_conditional_condition') ?? '';
    //$cardinality = $items->getFieldDefinition()->get('fieldStorage')->get('cardinality');
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()
                          ->getCardinality();
    if (!empty($condition)) {
      $conditions = explode("\r\n", $condition);
      foreach ($conditions as $cond) {
        if ($cardinality === 1) {
          $name_item = trim($items->getName()) . '[0][target_id]';
        }
        // Otherwise display a placeholder containing all the values in one field.
        else {
          $name_item = trim($items->getName()) . '[0][placeholder][]';
        }
        $c = explode("|", $cond);
        $element['#attached']['drupalSettings']['epp_conditional'][] = [
          'name_field_condition' => trim($c[0]),
          'value' => trim($c[1]),
          'vocabulary' => trim($c[2]),
          'name_item' => $name_item
        ];
      }
    }
    return $element;
  }
}
