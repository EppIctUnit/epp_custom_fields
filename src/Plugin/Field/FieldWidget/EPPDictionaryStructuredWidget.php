<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'epp_dictionary' structured widget.
 *
 * @FieldWidget(
 *   id = "epp_dictionary_structured_widget",
 *   label = @Translation("EPP dictionary widget - Structured"),
 *   field_types = {
 *     "epp_dictionary"
 *   }
 * )
 */
class EPPDictionaryStructuredWidget extends EPPDictionaryDefaultWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'structured_conditional_allow_parent' => FALSE,
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $element['structured_conditional_allow_parent'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow parent selection'),
      '#default_value' => $this->getSetting('structured_conditional_allow_parent') ?? FALSE,
    ];
    return $element;
  }

  /**
   * Get a hierarchical options list from taxonomy vocabularies.
   *
   * @param string $vocabulary
   *   The vocabulary.
   * @param array $settings
   *   The field settings.
   *
   * @return array
   *   Options with optionsets.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTaxonomyOptions($vocabulary, array $settings, array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\taxonomy\TermStorageInterface $term_storage */
    $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');
    $options = $parents = $children = [];
    $terms = $term_storage->loadTree($vocabulary, 0, NULL, TRUE);

    // Filter countries options if the access delegation option is checked.
    $accessDelegation = $vocabulary == 'countries' && !empty($settings['handler_settings']['access_delegation_container']['access_delegation']);

    // First store the parents and children information separately to handle
    // optgroups for hierarchical vocabularies.
    $tids = [];
    foreach ($terms as $term) {
      if (!$accessDelegation) {
        if (!empty($term->field_epp_formatted_value)) {
          $formatted_value = $term->field_epp_formatted_value->getValue();
        }
        if ($term->depth === 0) {
          $parents[$term->id()] = $formatted_value[0]['safe_value'] ?? $formatted_value[0]['value'] ?? $term->label();
        }
        else {
          $allow = $this->getSetting('structured_conditional_allow_parent') ?? FALSE;
          if ($allow) {
            $value = $formatted_value[0]['safe_value'] ?? $formatted_value[0]['value'] ?? $term->label();
            $tiret = '';
            foreach ($term->parents as $p) {
              $tiret .= '-';
            }
            $parents[$term->id()] = ' ' . $tiret . ' ' . $value;
          }
          else {
            $parent = $term->parents[0];
            $children[$parent][$term->id()] = $formatted_value[0]['safe_value'] ?? $formatted_value[0]['value'] ?? $term->label();
          }
        }
        $tids[$term->id()] = $vocabulary;
      }
    }

    // If the vocabulary is actually flat, no need for further work.
    if (empty($children)) {
      $options = $parents;
    }
    // Replace the parents term ids with their names for proper display.
    else {
      foreach ($children as $parent => $child) {
        if (isset($parents[$parent])) {
          $options[$parents[$parent]] = $child;
        }
      }
    }

    // Special case for bodies: unset the term if the related node committee
    // is marked as former.
    if ($vocabulary === 'bodies') {
      $options = $this->filterBodiesOptions($options);
    }
    $options = [
      'optionsList' => $options,
      'termsListByVocabulary' => $tids
    ];
    return $options;
  }

}
