<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\text\Plugin\Field\FieldWidget\TextareaWidget;

/**
 * Plugin implementation of the 'epp_formatted_value_widget' widget.
 *
 * @FieldWidget(
 *   id = "epp_formatted_value_widget",
 *   label = @Translation("EPP Formatted Value widget"),
 *   field_types = {
 *     "epp_formatted_value"
 *   }
 * )
 */
class EPPFormattedValueWidget extends TextareaWidget {

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $values = parent::massageFormValues($values, $form, $form_state);

    foreach ($values as &$value) {
      $value['safe_value'] = Xss::filter($value['value']);
    }

    return $values;
  }

}
