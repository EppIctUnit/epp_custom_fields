<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetBaseInterface;

/**
 * Plugin implementation of the 'epp_contact' widget.
 *
 * @FieldWidget(
 *   id = "epp_free_text_item_widget",
 *   label = @Translation("EPP Free Text item widget"),
 *   field_types = {
 *     "epp_free_text_item"
 *   }
 * )
 */
class EPPFreeTextItemWidget extends WidgetBase implements WidgetBaseInterface {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality == 1) {
      $element['#type'] = 'fieldset';
      $element['#collapsible'] = TRUE;
      $element['#collapsed'] = FALSE;
    }

    $element['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#maxlength' => 150,
      '#default_value' => $items[$delta]->description ?? NULL,
    ];

    $element['text'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Text for free text item'),
      '#default_value' => $items[$delta]->text ?? NULL,
      '#format' => $items[$delta]->text_format ?? 'filtered_html',
      '#base_type' => 'textarea',
    ];

    // As the widget has multiple fields, we need to use a custom
    // validation method to provide a better understanding error message.
    $element['#element_validate'][] = [
      static::class,
      'validateElement',
    ];

    return $element;
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if (!empty($element['#required']) && empty($element['text']['value']['#value'])) {
      $form_state->setError($element['text'], t('The Text for free text item of the @name field is required.', ['@name' => $element['#title']]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $key => $value) {
      $values[$key]['text'] = $value['text']['value'];
      $values[$key]['text_format'] = $value['text']['format'];
    }
    return $values;
  }

}
