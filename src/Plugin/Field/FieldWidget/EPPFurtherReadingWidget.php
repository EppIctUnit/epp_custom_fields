<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'epp_further_reading' widget.
 *
 * @FieldWidget(
 *   id = "epp_further_reading_widget",
 *   label = @Translation("EPP  Further reading widget"),
 *   field_types = {
 *     "epp_further_reading"
 *   }
 * )
 */
class EPPFurtherReadingWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality == 1) {
      $element['#type'] = 'fieldset';
      $element['#collapsible'] = TRUE;
      $element['#collapsed'] = FALSE;
    }

    $element['reading_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Further reading title'),
      '#size' => 60,
      '#maxlength' => 150,
      '#default_value' => $items[$delta]->reading_text ?? NULL,
      '#attributes' => [
        'class' => [
          'further-reading',
        ],
      ],
    ];

    $element['reading_link'] = [
      '#type' => 'textfield',
      '#title' => 'Further reading link',
      '#size' => 60,
      '#maxlength' => 500,
      '#default_value' => $items[$delta]->reading_link ?? NULL,
      '#attributes' => [
        'class' => [
          'further-reading-link',
        ],
      ],
    ];

    $element['target'] = [
      '#prefix' => '<div class="container-inline">',
      '#suffix' => '</div>',
      '#type' => 'radios',
      '#title' => 'Target',
      '#size' => 2,
      '#multiple' => 'FALSE',
      '#default_value' => $items[$delta]->target ?? '_blank',
      '#options' => [
        '_blank' => $this->t('New page'),
        '_top' => $this->t('Same page'),
      ],
      '#attributes' => [
        'class' => [
          'target-style',
        ],
      ],
    ];

    // As the widget has multiple fields, we need to use a custom
    // validation method to provide a better understanding error message.
    $element['#element_validate'][] = [
      static::class,
      'validateElement',
    ];

    return $element;
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if (!empty($element['#required'])
      && (empty($element['reading_text']['#value'])
      || empty($element['reading_link']['#value'])
      || empty($element['target']['#value']))
      ) {
      $form_state->setError($element, t('@name field is required.', ['@name' => $element['#title']]));
    }
  }

}
