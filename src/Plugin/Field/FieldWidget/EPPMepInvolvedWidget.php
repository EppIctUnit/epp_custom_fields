<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'epp_mep_involved' widget.
 *
 * @FieldWidget(
 *   id = "epp_mep_involved_widget",
 *   label = @Translation("Epp Mep involved widget"),
 *   field_types = {
 *     "epp_mep_involved"
 *   }
 * )
 */
class EPPMepInvolvedWidget extends WidgetBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * EPPMepInvolvedWidget constructor extends WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityTypeManagerInterface $entity_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality === 1) {
      $element['#type'] = 'fieldset';
      $element['#collapsible'] = TRUE;
      $element['#collapsed'] = FALSE;
    }

    $fields = [
      'role' => $this->t("MEP's Role reference"),
      'node' => $this->t("MEP's Name reference"),
    ];
    $settings = $this->getFieldSettings();
    foreach ($fields as $machine_name => $field_label) {
      // Get the existing value, if any, for the entity reference field.
      $target_type = $settings[$machine_name . '_target_type'];
      $target_id_setting = $machine_name . '_target_id';
      $default = isset($items[$delta]) ? $items[$delta]->$target_id_setting : NULL;
      $handler_settings = $settings[$machine_name]['handler_settings'];

      // If $machine_name === role construct role_code field.
      if ($machine_name === 'role') {
        $vids = $handler_settings['target_bundles'];
        $options = [];
        foreach ($vids as $vid) {
          $terms = $this->entityManager
            ->getStorage('taxonomy_term')
            ->loadTree($vid);
          foreach ($terms as $term) {
            if (!empty(trim($term->name)) && !empty(trim($term->description__value))) {
              $options[$term->name] = ucfirst($term->description__value);
            }
          }
        }
        if (!empty($options)) {
          asort($options);
        }

        // Construct select role_code field.
        $element['role_code'] = [
          '#type' => 'select',
          '#title' => $this->t("MEP's Role"),
          '#options' => $options,
          '#default_value' => $items[$delta]->role_code ?? '',
          '#empty_option' => $this->t('No role'),
        ];
      }

      if (!empty($default)) {
        $default = $this->entityManager
          ->getStorage($target_type)
          ->load($default);
      }

      $element[$machine_name . '_target_id'] = [
        '#type' => 'entity_autocomplete',
        '#title' => $field_label,
        '#target_type' => $target_type,
        '#selection_handler' => $settings[$machine_name]['handler'] ?? 'default',
        '#selection_settings' => $handler_settings,
        '#default_value' => $default ?? NULL,
      ];
      if ('node' === $machine_name) {
        // Use a custom entity reference selection handler to alter the underlying query.
        $element[$machine_name . '_target_id']['#selection_handler'] = 'default:mep_by_former_status';
      }

      // If field role_target_id make readonly field and add description.
      if ($machine_name === 'role') {
        $element[$machine_name . '_target_id']['#access'] = FALSE;
        $element[$machine_name . '_target_id']['#description'] = $this->t('This field is a technical field, it is automatically filled');
      }
    }

    // As the widget has multiple fields, we need to use a custom
    // validation method to provide a better understanding error message.
    $element['#element_validate'][] = [
      static::class,
      'validateElement',
    ];

    return $element;
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if (!empty($element['#required']) && empty($element['node_target_id']['#value'])) {
      $form_state->setError($element['node_target_id'], t("The MEP's Name reference of the @name field is required.", ['@name' => $element['#title']]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // Auto filled field role_target_id.
    foreach ($values as &$item) {
      $item['role_target_id'] = '';
      if (!empty($item['role_code'])) {
        $properties = [
          'name' => $item['role_code'],
          'vid' => "meps_involved_roles",
        ];
        $terms = $this->entityManager
          ->getStorage('taxonomy_term')
          ->loadByProperties($properties);
        $tid = key($terms);
        $item['role_target_id'] = $tid;
      }
      else {
        unset($item['role_target_id'], $item['role_code']);
      }
    }
    return $values;
  }

}
