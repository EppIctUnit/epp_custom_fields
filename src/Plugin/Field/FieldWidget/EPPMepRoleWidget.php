<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Render\Element\Number;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'epp_mep_role' widget.
 *
 * @FieldWidget(
 *   id = "epp_mep_role_widget",
 *   label = @Translation("EPP MEP Role widget"),
 *   field_types = {
 *     "epp_mep_role"
 *   }
 * )
 */
class EPPMepRoleWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality == 1) {
      $element['#type'] = 'fieldset';
      $element['#collapsible'] = TRUE;
      $element['#collapsed'] = FALSE;
    }

    $element['code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Role code'),
      '#default_value' => $items[$delta]->code ?? NULL,
    ];

    $element['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Role name'),
      '#default_value' => $items[$delta]->name ?? NULL,
    ];

    $element['weight'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Role precedence'),
      '#default_value' => $items[$delta]->weight ?? 0,
      '#element_validate' => [
        [Number::class, 'validateNumber'],
      ],
    ];

    // As the widget has multiple fields, we need to use a custom
    // validation method to provide a better understanding error message.
    $element['#element_validate'][] = [
      static::class,
      'validateElement',
    ];

    return $element;
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if (!empty($element['#required'])
      && (empty($element['code']['#value'])
      || empty($element['name']['#value'])
      || (empty($element['weight']['#value']) && $element['weight']['#value'] != '0'))
    ) {
      $form_state->setError($element, t('@name field is required.', ['@name' => $element['#title']]));
    }
  }

}
