<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Render\Element\Number;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'epp_mep_status' widget.
 *
 * @FieldWidget(
 *   id = "epp_mep_status_widget",
 *   label = @Translation("EPP MEP Status widget"),
 *   field_types = {
 *     "epp_mep_status"
 *   }
 * )
 */
class EPPMepStatusWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality == 1) {
      $element['#type'] = 'fieldset';
      $element['#collapsible'] = TRUE;
      $element['#collapsed'] = FALSE;
    }

    $element['presidence_member'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is Presidence Member'),
      '#maxlength' => 10,
      '#default_value' => !empty($items[$delta]->presidence_member) ? 1 : 0,
      '#element_validate' => [
        [Number::class, 'validateNumber'],
      ],
    ];

    $element['presidence_function'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Function in Presidence'),
      '#maxlength' => 255,
      '#default_value' => $items[$delta]->presidence_function ?? NULL,
    ];

    $element['bureau_member'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is Bureau Member'),
      '#maxlength' => 10,
      '#default_value' => !empty($items[$delta]->bureau_member) ? 1 : 0,
      '#element_validate' => [
        [Number::class, 'validateNumber'],
      ],
    ];

    $element['bureau_function'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Function in Bureau'),
      '#maxlength' => 255,
      '#default_value' => $items[$delta]->bureau_function ?? NULL,
    ];

    $element['nat_del_head'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is Head of National Delegation'),
      '#maxlength' => 10,
      '#default_value' => !empty($items[$delta]->nat_del_head) ? 1 : 0,
      '#element_validate' => [
        [Number::class, 'validateNumber'],
      ],
    ];

    $element['former'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Is Former Member'),
      '#maxlength' => 10,
      '#default_value' => !empty($items[$delta]->former) ? 1 : 0,
      '#element_validate' => [
        [Number::class, 'validateNumber'],
      ],
    ];

    $element['nat_del_weight'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Precedence in National Delegation'),
      '#maxlength' => 11,
      '#default_value' => $items[$delta]->nat_del_weight ?? 0,
      '#element_validate' => [
        [Number::class, 'validateNumber'],
      ],
    ];

    // As the widget has multiple fields, we need to use a custom
    // validation method to provide a better understanding error message.
    $element['#element_validate'][] = [
      static::class,
      'validateElement',
    ];

    return $element;
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if (!empty($element['#required'])
      && (empty($element['nat_del_weight']['#value'])
      && ($element['nat_del_weight']['#value'] != '0'))
    ) {
      $form_state->setError($element['nat_del_weight'], t('The Precedence in National Delegation of the @name field is required.', ['@name' => $element['#title']]));
    }
  }

}
