<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Render\Element\Number;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'epp_metadata' widget.
 *
 * @FieldWidget(
 *   id = "epp_metadata_widget",
 *   label = @Translation("EPP Metadata widget"),
 *   field_types = {
 *     "epp_metadata"
 *   }
 * )
 */
class EPPMetadataWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality == 1) {
      $element['#type'] = 'fieldset';
      $element['#collapsible'] = TRUE;
      $element['#collapsed'] = FALSE;
    }

    $element['body_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Body code'),
      '#default_value' => $items[$delta]->body_code ?? NULL,
      '#required' => $this->fieldDefinition->isRequired(),
    ];

    $element['body_weight'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Body precedence'),
      '#default_value' => $items[$delta]->body_weight ?? 0,
      '#element_validate' => [
        [Number::class, 'validateNumber'],
      ],
      '#required' => $this->fieldDefinition->isRequired(),
    ];

    $element['bodytype_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Body type code'),
      '#default_value' => $items[$delta]->bodytype_code ?? NULL,
      '#required' => $this->fieldDefinition->isRequired(),
    ];

    $element['bodytype_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Body type name'),
      '#default_value' => $items[$delta]->bodytype_name ?? NULL,
      '#required' => $this->fieldDefinition->isRequired(),
    ];

    $element['bodytype_weight'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Body type precedence'),
      '#default_value' => $items[$delta]->bodytype_weight ?? 0,
      '#element_validate' => [
        [Number::class, 'validateNumber'],
      ],
      '#required' => $this->fieldDefinition->isRequired(),
    ];

    // As the widget has multiple fields, we need to use a custom
    // validation method to provide a better understanding error message.
    $element['#element_validate'][] = [
      static::class,
      'validateElement',
    ];

    return $element;
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if (!empty($element['#required'])) {
      if (empty($element['body_code']['#value'])
        || empty($element['bodytype_code']['#value'])
        || empty($element['bodytype_name']['#value'])
        || (empty($element['body_weight']['#value']) && $element['body_weight']['#value'] != '0')
        || (empty($element['bodytype_weight']['#value']) && $element['bodytype_weight']['#value'] != '0')
      ) {
        $form_state->setError($element['body_code'], t('@name field is required.', ['@name' => $element['#title']]));
        $form_state->setError($element['bodytype_code']);
        $form_state->setError($element['bodytype_name']);
      }
    }

  }

}
