<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'epp_names' widget.
 *
 * @FieldWidget(
 *   id = "epp_names_widget",
 *   label = @Translation("EPP Names widget"),
 *   field_types = {
 *     "epp_names"
 *   }
 * )
 */
class EPPNamesWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality === 1) {
      $element['#type'] = 'fieldset';
      $element['#collapsible'] = TRUE;
      $element['#collapsed'] = FALSE;
    }

    $element['first'] = [
      '#type' => 'textfield',
      '#title' => $this->t('First name'),
      '#maxlength' => 50,
      '#default_value' => $items[$delta]->first ?? NULL,
    ];
    $element['last'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Last name'),
      '#maxlength' => 100,
      '#default_value' => $items[$delta]->last ?? NULL,
    ];
    $element['full'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Full name'),
      '#maxlength' => 150,
      '#default_value' => $items[$delta]->full ?? NULL,
    ];
    $element['sort'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sort name'),
      '#maxlength' => 150,
      '#default_value' => $items[$delta]->sort ?? NULL,
    ];

    // As the widget has multiple fields, we need to use a custom
    // validation method to provide a better understanding error message.
    $element['#element_validate'][] = [
      static::class,
      'validateElement',
    ];

    return $element;
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if (!empty($element['#required'])
      && (empty($element['first']['#value'])
      && empty($element['last']['#value'])
      && empty($element['full']['#value']))
    ) {
      $form_state->setError($element['first'], t('At least one field among First name, Last name and Full name of the @name field is required.', ['@name' => $element['#title']]));
      $form_state->setError($element['last']);
      $form_state->setError($element['full']);
    }
  }

}
