<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'mep_title' widget.
 *
 * @FieldWidget(
 *   id = "epp_picture_widget",
 *   label = @Translation("EPP Picture widget"),
 *   field_types = {
 *     "epp_picture"
 *   }
 * )
 */
class EPPPictureWidget extends WidgetBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The database connexion.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * EPPMepInvolvedWidget constructor extends WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connexion.
   */
  public function __construct($plugin_id,
                              $plugin_definition,
                              FieldDefinitionInterface $field_definition,
                              array $settings,
                              array $third_party_settings,
                              EntityTypeManagerInterface $entity_type_manager,
                              EntityFieldManagerInterface $entity_field_manager,
                              Connection $database) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);

    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'use_file_widget' => FALSE,
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element['use_file_widget'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use file widget instead of node reference'),
      '#default_value' => $this->getSetting('use_file_widget'),
      '#description' => $this->t('Replace the node reference field with a standard upload file widget.'),
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality === 1) {
      $element['#type'] = 'fieldset';
      $element['#collapsible'] = TRUE;
      $element['#collapsed'] = FALSE;
    }

    $element['alternate'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alternate'),
      '#maxlength' => 500,
      '#default_value' => $items[$delta]->alternate ?? NULL,
      '#prefix' => '<div id="epp-picture-alternate-' . $items->getName() . '">',
      '#suffix' => '</div>',
    ];

    $element['description'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Description'),
      '#default_value' => $items[$delta]->description ?? '',
      '#rows' => $this->getSetting('rows'),
      '#placeholder' => $this->getSetting('placeholder'),
      '#attributes' => ['class' => ['js-text-full', 'text-full']],
      '#format' => $items[$delta]->description_format ?? 'full_html',
      '#base_type' => 'textarea',
    ];

    // Get the existing value, if any, for the entity reference field.
    $target_type = $this->getFieldSetting('target_type');
    $default = isset($items[$delta]) ? $items[$delta]->target_id : NULL;
    if (!empty($element['#field_parents']) && !empty($items->getParent()->getEntity()->getEntityType()->id())) {
      $entity_type = $items->getParent()->getEntity()->getEntityType()->id();
      if (
        !empty($items->getParent()->getEntity()->get($items->getName())) &&
        !empty($items->getParent()->getEntity()->get($items->getName())->getValue()[$delta]) &&
        !empty($items->getParent()->getEntity()->get($items->getName())->getValue()[$delta]['target_id'])
      ) {
        $default = $default_id = $items->getParent()->getEntity()->get($items->getName())->getValue()[$delta]['target_id'];
      }
    }
    if (!empty($default)) {
      $default = $this->entityTypeManager
        ->getStorage($target_type)
        ->load($default);
    }

    if ($this->getSetting('use_file_widget')) {
      $element['fid'] = [
        '#type' => 'managed_file',
        '#title' => $this->t('Picture'),
        '#upload_location' => 'public://photo/' . date('Y') . '/' . date('m'),
        '#upload_validators' => [
          'file_validate_extensions' => ['jpg jpeg'],
        ],
        '#progress_indicator' => 'throbber',
        '#default_value' => !empty($items[$delta]->fid) ? [$items[$delta]->fid] : NULL,
        '#required' => $this->fieldDefinition->isRequired(),
      ];
    }
    else {
      $element['target_id'] = [
        '#type' => 'entity_autocomplete',
        '#title' => $this->t('Picture'),
        '#target_type' => $this->getFieldSetting('target_type'),
        '#selection_handler' => $this->getFieldSetting('handler'),
        '#selection_settings' => $this->getFieldSetting('handler_settings'),
        '#default_value' => $default ?? NULL,
        '#required' => $this->fieldDefinition->isRequired(),
        '#maxlength' => 500,
        '#ajax' => [
          'wrapper' => 'epp-picture-alternate-' . $items->getName(),
          'callback' => '\Drupal\epp_custom_fields\Plugin\Field\FieldWidget\EPPPictureWidget::alternateAjaxCallback',
          'event' => 'autocompleteclose',
        ],
        '#limit_validation_errors' => [[
          $items->getName(),
        ]],
        /*'#attributes' => [
          'style' => 'display: none',
        ],*/
      ];

      if (
        ($input = $form_state->getUserInput())
        && !is_null($default)
        && !empty($input[$items->getName()])
        && ($picture = $input[$items->getName()])
        && !empty($picture[0]['target_id'])
        && ($entity_type =  $entity_type ?? $form_state->getFormObject()->getEntity()->getEntityTypeId())
        && ($target_id = ($entity_type === 'paragraph' && !empty($default_id) ? $default_id : EntityAutocomplete::extractEntityIdFromAutocompleteInput($picture[0]['target_id'])))
        && ($langcode = $form_state->getFormObject()->getEntity()->langcode->value)
        && ($bundle = $form_state->getFormObject()->getEntity()->bundle())
        && ($defaultAlternate = $this->getDefaultAlternate($target_type, $target_id, $langcode, $entity_type, $bundle))
      ) {
        $element['alternate']['#value'] = $defaultAlternate['value'];
        $element['alternate']['#default_value'] = $defaultAlternate['value'];
      }
    }

    // As the widget has multiple fields, we need to use a custom
    // validation method to provide a better understanding error message.
    $element['#element_validate'][] = [
      static::class,
      'validateElement',
    ];

    return $element;
  }

  /**
   * Ajax callback for the autocomplete field.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The state of the form.
   *
   * @return array
   *   The alternate field updated.
   */
  public static function alternateAjaxCallback(array $form, FormStateInterface $form_state) {
    $triggering = $form_state->getTriggeringElement();
    $alternate = $form;
    $stopAt = count($triggering['#array_parents']) - 1;
    foreach ($triggering['#array_parents'] as $index => $parent) {
      if ($index < $stopAt) {
        $alternate = $alternate[$parent];
      }
      else {
        $alternate = $alternate['alternate'];
      }
    }
    return $alternate;
  }

  /**
   * Get a default value for the alternate field.
   *
   * @param string $target_type
   *   The target type of aut the autocomplete field.
   * @param int $target_id
   *   The id of the targeted entity in the autocomplete field.
   * @param string $langcode
   *   The language of the entity beeing created with the form.
   * @param string $entity_type
   *   The type of the entity beeing created with the form.
   * @param string $bundle
   *   The bundle of the entity beeing created with the form.
   *
   * @return array
   *   The alternate default value.
   */
  public function getDefaultAlternate($target_type, $target_id, $langcode, $entity_type, $bundle) {
    $alternate = [];

    // First, try to find an alternate override in the same bundle and language.
    $defs = $this->entityFieldManager->getFieldMapByFieldType('epp_picture');

    foreach ($defs[$entity_type] as $field_name => $field_info) {
      $result = $this->database->select($entity_type . '__' . $field_name, 'f')
        ->fields('f', ['entity_id', $field_name . '_alternate'])
        ->condition('langcode', $langcode)
        ->condition('bundle', $bundle)
        ->condition($field_name . '_target_id', $target_id)
        ->isNotNull($field_name . '_alternate')
        ->condition($field_name . '_alternate', '', '!=')
        ->orderBy('entity_id', 'desc')
        ->range(0, 1)
        ->execute()
        ->fetchAll();

      if (!empty($result)
        && (empty($alternate['value']) || ($alternate['entity_id'] < $result[0]->entity_id))
      ) {
        $alternate = [
          'entity_id' => $result[0]->entity_id,
          'value' => $result[0]->{$field_name . '_alternate'},
        ];
      }
    }

    // If no result was found,
    // try to find an alternate override in the same language in other bundles.
    if (empty($alternate)) {
      foreach ($defs[$entity_type] as $field_name => $field_info) {
        $result = $this->database->select($entity_type . '__' . $field_name, 'f')
          ->fields('f', ['entity_id', $field_name . '_alternate'])
          ->condition('langcode', $langcode)
          ->condition('bundle', $bundle, '!=')
          ->condition($field_name . '_target_id', $target_id)
          ->condition($field_name . '_alternate', '', '!=')
          ->isNotNull($field_name . '_alternate')
          ->orderBy('entity_id', 'desc')
          ->range(0, 1)
          ->execute()
          ->fetchAll();

        if (!empty($result)
          && $result[0]->{$field_name . '_alternate'} != $target_id
          && (empty($alternate['value']) || ($alternate['entity_id'] < $result[0]->entity_id))
        ) {
          $alternate = [
            'entity_id' => $result[0]->entity_id,
            'value' => $result[0]->{$field_name . '_alternate'},
          ];
        }
      }
    }

    // If no result was found get the original alternate from the target entity.
    if (empty($alternate) || (isset($alternate['value']) && $target_id === $alternate['value'])) {
      $alternate['value'] = '';
      $settings = $this->getFieldSettings();
      $picture = $this->entityTypeManager->getStorage($target_type)->load($target_id);
      $target_bundle = $picture->bundle();
      $alternate_field = $settings['handler_settings'][$target_bundle . '_alt_field'] ?? FALSE;

      if ($alternate_field == 'nid') {
        $alternate_field = '';
        foreach (array_keys($picture->getFields()) as $field_name) {
          if (strpos($field_name, '_alt') !== false) {
            $alternate_field = $field_name;
            break;
          }
        }
      }

      if ($alternate_field && $picture->hasField($alternate_field)) {
        if ($picture->hasTranslation($langcode)) {
          $alternate['value'] = $picture->getTranslation($langcode)
            ->get($alternate_field)->value;
        }
        elseif ($picture->hasTranslation('en')) {
          $alternate['value'] = $picture->getTranslation('en')
            ->get($alternate_field)->value;
        }
      }
    }

    return $alternate;
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if (!empty($element['#required']) && empty($element['target_id']['#value']) && empty($element['fid']['#value'])) {
      $subElement = $element['target_id'] ?? $element['fid'];
      $form_state->setError($subElement, t('The Picture of the @name field is required.', ['@name' => $element['#title']]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // First we build the list of fields that can cause this issue: optional
    // integers or references fields.
    $field_definition = $this->fieldDefinition->getFieldStorageDefinition();
    $property_definitions = $field_definition->getPropertyDefinitions();
    $to_unset = [];

    // Help for search real field values.
    $user_input = $form_state->getUserInput();
    $path = array_merge($form['#parents'], [$this->fieldDefinition->getName()]);
    $real_values = $this->getValueFromPathField($user_input, $path);

    // Map description value and format with their columns.
    foreach ($values as $delta => $value) {
      $values[$delta]['description_format'] = $values[$delta]['description']['format'];
      $values[$delta]['description'] = $values[$delta]['description']['value'];

      if (!empty($values[$delta]['fid'][0])) {
        $values[$delta]['fid'] = $values[$delta]['fid'][0];
      }
      elseif (
        !empty($values[$delta]['target_id'])
        && ($node = $this->entityTypeManager->getStorage('node')->load($values[$delta]['target_id']))
        && !empty($node->field_image->target_id)
      ) {
        $values[$delta]['fid'] = $node->field_image->target_id;
      }
      if (!empty($real_values[$delta]['alternate'])) {
        $values[$delta]['alternate'] = $real_values[$delta]['alternate'];
      }
      elseif (
        !empty($values[$delta]['target_id'])
        && ($node = $this->entityTypeManager->getStorage('node')->load($values[$delta]['target_id']))
      ) {
        foreach (array_keys($node->getFields()) as $field_name) {
          if (strpos($field_name, '_alt') !== false) {
            if ($node->hasField($field_name)) {
              $values[$delta]['alternate'] = $node->get($field_name)->value;
              break;
            }
          }
        }
      }
    }

    foreach ($property_definitions as $field_name => $property_definition) {
      if (!$property_definition->isRequired() &&
        ($property_definition instanceof DataReferenceTargetDefinition ||
          ($property_definition instanceof DataDefinition && $property_definition->getDataType() === 'integer'))) {
        $to_unset[] = $field_name;
      }
    }

    // Then we check our form values to see if these fields are empty to safely
    // unset them and prevent the apparition of the error.
    $values = parent::massageFormValues($values, $form, $form_state);
    foreach ($values as $delta => $value) {
      foreach ($value as $field_name => $field_value) {
        if (empty($field_value) && in_array($field_name, $to_unset, FALSE)) {
          unset($values[$delta][$field_name]);
        }
      }
    }
    return $values;
  }

  /**
   * Get value from path field.
   *
   * @param array $array
   *   The entry table.
   * @param array $path
   *   The path to the value in table form.
   *
   * @return mixed
   *   The value found, or NULL if the path does not exist.
   */
  private function getValueFromPathField(array $array, array $path) {
    $current = $array;
    foreach ($path as $key) {
      if (!isset($current[$key])) {
        return NULL;
      }
      $current = $current[$key];
    }
    return $current;
  }

}
