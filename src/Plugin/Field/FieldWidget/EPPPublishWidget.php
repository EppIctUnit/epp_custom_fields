<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'epp_publish' widget.
 *
 * @FieldWidget(
 *   id = "epp_publish_widget",
 *   label = @Translation("EPP Publish widget"),
 *   field_types = {
 *     "epp_publish"
 *   }
 * )
 */
class EPPPublishWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality === 1) {
      $element['#type'] = 'fieldset';
      $element['#collapsible'] = TRUE;
      $element['#collapsed'] = FALSE;
    }

    $element['general'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Can be published'),
      '#default_value' => $items[$delta]->general ?? 0,
    ];
    $element['intranet'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Can be published on intranet'),
      '#default_value' => $items[$delta]->intranet ?? 0,
    ];
    $element['internet'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Can be published on Internet'),
      '#default_value' => $items[$delta]->internet ?? 0,
    ];
    $element['gsm'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('GSM can be published on Internet'),
      '#default_value' => $items[$delta]->gsm ?? 0,
    ];
    $element['picture'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('picture can be published on Internet'),
      '#default_value' => $items[$delta]->picture ?? 0,
    ];

    // As the widget has multiple fields, we need to use a custom
    // validation method to provide a better understanding error message.
    $element['#element_validate'][] = [
      static::class,
      'validateElement',
    ];

    return $element;
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if (!empty($element['#required'])
      && (empty($element['general']['#value'])
      && empty($element['intranet']['#value'])
      && empty($element['internet']['#value'])
      && empty($element['gsm']['#value'])
      && empty($element['picture']['#value']))
    ) {
      $form_state->setError($element, t('At least one field of the @name field is required.', ['@name' => $element['#title']]));
    }
  }

}
