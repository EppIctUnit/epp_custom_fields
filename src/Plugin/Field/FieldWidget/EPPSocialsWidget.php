<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'epp_socials' widget.
 *
 * @FieldWidget(
 *   id = "epp_socials_widget",
 *   label = @Translation("EPP Socials widget"),
 *   field_types = {
 *     "epp_socials"
 *   }
 * )
 */
class EPPSocialsWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality === 1) {
      $element['#type'] = 'fieldset';
      $element['#collapsible'] = TRUE;
      $element['#collapsed'] = FALSE;
    }

    $element['twitter_handle'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Twitter handle'),
      '#maxlength' => 50,
      '#default_value' => $items[$delta]->twitter_handle ?? NULL,
    ];
    $element['facebook'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Facebook URL'),
      '#maxlength' => 255,
      '#default_value' => $items[$delta]->facebook ?? NULL,
    ];
    $element['pinterest'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pinterest URL'),
      '#maxlength' => 255,
      '#default_value' => $items[$delta]->pinterest ?? NULL,
    ];
    $element['instagram'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Instagram URL'),
      '#maxlength' => 255,
      '#default_value' => $items[$delta]->instagram ?? NULL,
    ];
    $element['youtube'] = [
      '#type' => 'textfield',
      '#title' => $this->t('YouTube URL'),
      '#maxlength' => 255,
      '#default_value' => $items[$delta]->youtube ?? NULL,
    ];
    $element['linkedin'] = [
      '#type' => 'textfield',
      '#title' => $this->t('LinkedIn URL'),
      '#maxlength' => 255,
      '#default_value' => $items[$delta]->linkedin ?? NULL,
    ];
    $element['flicker'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Flicker URL'),
      '#maxlength' => 255,
      '#default_value' => $items[$delta]->flicker ?? NULL,
    ];

    // As the widget has multiple fields, we need to use a custom
    // validation method to provide a better understanding error message.
    $element['#element_validate'][] = [
      static::class,
      'validateElement',
    ];

    return $element;
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if (!empty($element['#required'])
      && (empty($element['twitter_handle']['#value'])
      && empty($element['facebook']['#value'])
      && empty($element['pinterest']['#value'])
      && empty($element['instagram']['#value'])
      && empty($element['youtube']['#value'])
      && empty($element['linkedin']['#value'])
      && empty($element['flicker']['#value']))
    ) {
      $form_state->setError($element, t('At least one field of the @name field is required.', ['@name' => $element['#title']]));
    }
  }

}
