<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Render\Element\Number;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'epp_staff_service' widget.
 *
 * @FieldWidget(
 *   id = "epp_staff_service_widget",
 *   label = @Translation("EPP Staff Service widget"),
 *   field_types = {
 *     "epp_staff_service"
 *   }
 * )
 */
class EPPStaffServiceWidget extends WidgetBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * EPPMepInvolvedWidget constructor extends WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality === 1) {
      $element['#type'] = 'fieldset';
      $element['#collapsible'] = TRUE;
      $element['#collapsed'] = FALSE;
    }

    $element['short_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service short name'),
      '#maxlength' => 250,
      '#default_value' => $items[$delta]->short_name ?? NULL,
    ];

    $element['long_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service long name'),
      '#maxlength' => 250,
      '#default_value' => $items[$delta]->long_name ?? NULL,
    ];

    $element['type'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service type'),
      '#default_value' => $items[$delta]->type ?? NULL,
    ];

    $element['service_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Service code'),
      '#default_value' => $items[$delta]->service_code ?? 0,
      '#element_validate' => [
        [Number::class, 'validateNumber'],
      ],
    ];

    $element['body_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Body code'),
      '#default_value' => $items[$delta]->body_code ?? NULL,
    ];

    // Get the existing value, if any, for the entity reference field.
    $target_type = $this->getFieldSetting('target_type');
    $default = isset($items[$delta]) ? $items[$delta]->target_id : NULL;
    if (!empty($default)) {
      $default = $this->entityTypeManager
        ->getStorage($target_type)
        ->load($default);
    }
    $element['target_id'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Body reference'),
      '#target_type' => $this->getFieldSetting('target_type'),
      '#selection_handler' => $this->getFieldSetting('handler'),
      '#selection_settings' => $this->getFieldSetting('handler_settings'),
      '#default_value' => $default ?? NULL,

    ];

    $element['important'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Importance for this service'),
      '#default_value' => !empty($items[$delta]->important) ? 1 : 0,
      '#element_validate' => [
        [Number::class, 'validateNumber'],
      ],
    ];

    $element['responsible'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Responsible for this service'),
      '#default_value' => !empty($items[$delta]->responsible) ? 1 : 0,
      '#element_validate' => [
        [Number::class, 'validateNumber'],
      ],
    ];

    $element['precedence'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Precedence in service'),
      '#default_value' => $items[$delta]->precedence ?? 0,
      '#element_validate' => [
        [Number::class, 'validateNumber'],
      ],
    ];

    $element['internet_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title for Internet'),
      '#maxlength' => 512,
      '#default_value' => $items[$delta]->internet_title ?? NULL,
    ];

    $element['internal_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title for EPP Group'),
      '#default_value' => $items[$delta]->internal_title ?? NULL,
    ];

    // As the widget has multiple fields, we need to use a custom
    // validation method to provide a better understanding error message.
    $element['#element_validate'][] = [
      static::class,
      'validateElement',
    ];

    return $element;
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if (!empty($element['#required'])) {
      if (empty($element['short_name']['#value'])) {
        $form_state->setError($element['short_name'], t('The Service short name of the @name field is required.', ['@name' => $element['#title']]));
      }

      if (empty($element['long_name']['#value'])) {
        $form_state->setError($element['long_name'], t('The Service long name of the @name field is required.', ['@name' => $element['#title']]));
      }

      if (empty($element['type']['#value'])) {
        $form_state->setError($element['type'], t('The Service type of the @name field is required.', ['@name' => $element['#title']]));
      }

      if (empty($element['service_code']['#value']) && ($element['service_code']['#value'] != '0')) {
        $form_state->setError($element['service_code'], t('The Service code of the @name field is required.', ['@name' => $element['#title']]));
      }

      if (empty($element['important']['#value']) && ($element['service_code']['#value'] != '0')) {
        $form_state->setError($element['important'], t('The Importance of the @name field is required.', ['@name' => $element['#title']]));
      }

      if (empty($element['responsible']['#value']) && ($element['service_code']['#value'] != '0')) {
        $form_state->setError($element['responsible'], t('The Responsible of the @name field is required.', ['@name' => $element['#title']]));
      }

      if (empty($element['precedence']['#value']) && ($element['service_code']['#value'] != '0')) {
        $form_state->setError($element['precedence'], t('The Precedence of the @name field is required.', ['@name' => $element['#title']]));
      }

      if (empty($element['internet_title']['#value'])) {
        $form_state->setError($element['internet_title'], t('The Title for Internet of the @name field is required.', ['@name' => $element['#title']]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // First we build the list of fields that can cause this issue: optional
    // integers or references fields.
    $field_definition = $this->fieldDefinition->getFieldStorageDefinition();
    $property_definitions = $field_definition->getPropertyDefinitions();
    $to_unset = [];
    foreach ($property_definitions as $field_name => $property_definition) {
      if (!$property_definition->isRequired() &&
        ($property_definition instanceof DataReferenceTargetDefinition ||
          ($property_definition instanceof DataDefinition && $property_definition->getDataType() === 'integer'))) {
        $to_unset[] = $field_name;
      }
    }

    // Then we check our form values to see if these fields are empty to safely
    // unset them and prevent the apparition of the error.
    $values = parent::massageFormValues($values, $form, $form_state);
    foreach ($values as $delta => $value) {
      foreach ($value as $field_name => $field_value) {
        if (empty($field_value) && in_array($field_name, $to_unset, FALSE)) {
          unset($values[$delta][$field_name]);
        }
      }
    }
    return $values;
  }

}
