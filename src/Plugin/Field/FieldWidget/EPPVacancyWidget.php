<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'epp_vacancy' widget.
 *
 * @FieldWidget(
 *   id = "epp_vacancy_widget",
 *   label = @Translation("EPP vacancy widget"),
 *   field_types = {
 *     "epp_vacancy"
 *   }
 * )
 */
class EPPVacancyWidget extends WidgetBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * EPPMepInvolvedWidget constructor extends WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality === 1) {
      $element['#type'] = 'fieldset';
      $element['#collapsible'] = TRUE;
      $element['#collapsed'] = FALSE;
    }

    $element['reference'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Vacancy reference'),
      '#size' => 30,
      '#maxlength' => 50,
      '#default_value' => $items[$delta]->reference ?? NULL,
      '#required' => $this->fieldDefinition->isRequired(),
    ];

    $element['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Job title'),
      '#size' => 60,
      '#maxlength' => 500,
      '#default_value' => $items[$delta]->description ?? NULL,
      '#required' => $this->fieldDefinition->isRequired(),
    ];

    $fields = [
      'grade' => $this->t('Grade'),
      'position_type' => $this->t('Position type'),
    ];
    $settings = $this->getFieldSettings();
    foreach ($fields as $machine_name => $field_label) {
      // Get the existing value, if any, for the entity reference field.
      $target_type = $settings[$machine_name . '_target_type'];
      $target_id_setting = $machine_name . '_target_id';
      $default = isset($items[$delta]) ? $items[$delta]->$target_id_setting : NULL;
      if (!empty($default)) {
        $default = $this->entityTypeManager
          ->getStorage($target_type)
          ->load($default);
      }
      $element[$machine_name . '_target_id'] = [
        '#type' => 'entity_autocomplete',
        '#title' => $field_label,
        '#target_type' => $target_type,
        '#selection_handler' => $settings[$machine_name]['handler'],
        '#selection_settings' => $settings[$machine_name]['handler_settings'],
        '#default_value' => $default ?? NULL,
        '#required' => $this->fieldDefinition->isRequired(),
      ];
    }

    $element['deadline'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Vacancy deadline'),
      '#date_time_element' => 'time',
      '#date_date_element' => 'date',
      '#default_value' => isset($items[$delta]->deadline) ? DrupalDateTime::createFromTimestamp($items[$delta]->deadline) : '',
      '#required' => $this->fieldDefinition->isRequired(),
    ];

    $element['competition_date'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Vacancy competition date'),
      '#default_value' => isset($items[$delta]->competition_date) ? $items[$delta]->competition_date : NULL,
    ];

    $element['result_date'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Vacancy result date'),
      '#default_value' => isset($items[$delta]->result_date) ? $items[$delta]->result_date : NULL,
    ];

    $element['minimum_salary'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Vacancy minimum salary'),
      '#default_value' => isset($items[$delta]->minimum_salary) ? $items[$delta]->minimum_salary : NULL,
      '#required' => FALSE,
    ];

    $element['apply_link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Link to application form'),
      '#size' => 60,
      '#maxlength' => 500,
      '#default_value' => $items[$delta]->apply_link ?? NULL,
      '#required' => $this->fieldDefinition->isRequired(),
    ];

    // As the widget has multiple fields, we need to use a custom
    // validation method to provide a better understanding error message.
    $element['#element_validate'][] = [
      static::class,
      'validateElement',
    ];

    return $element;
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if (!empty($element['#required'])
      && (empty($element['reference']['#value'])
      || empty($element['description']['#value'])
      || empty($element['grade_target_id']['#value'])
      || empty($element['position_type_target_id']['#value'])
      || empty($element['deadline']['#value'])
      || empty($element['apply_link']['#value']))
    ) {
      $form_state->setError($element, t('@name field is required.', ['@name' => $element['#title']]));
    }
  }

  /**
   * Transform our datetime value in timestamp for storage.
   *
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $values = parent::massageFormValues($values, $form, $form_state);
    $field_definition = $this->fieldDefinition->getFieldStorageDefinition();
    $property_definitions = $field_definition->getPropertyDefinitions();

    foreach ($values as $delta => $value) {
      foreach ($value as $field_name => &$field_value) {
        if (isset($property_definitions[$field_name])
          && $property_definitions[$field_name] instanceof DataDefinition
          && $property_definitions[$field_name]->getDataType() === 'timestamp'
          && $field_value && $field_value instanceof DrupalDateTime) {
          $values[$delta][$field_name] = $field_value->getTimestamp();
        }
      }
    }
    return $values;
  }

}
