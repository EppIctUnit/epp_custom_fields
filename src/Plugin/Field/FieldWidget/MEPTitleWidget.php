<?php

namespace Drupal\epp_custom_fields\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Render\Element\Number;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'mep_title' widget.
 *
 * @FieldWidget(
 *   id = "mep_title_widget",
 *   label = @Translation("MEP title widget"),
 *   field_types = {
 *     "mep_title"
 *   }
 * )
 */
class MEPTitleWidget extends WidgetBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * EPPMepInvolvedWidget constructor extends WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $cardinality = $this->fieldDefinition->getFieldStorageDefinition()
      ->getCardinality();

    if ($cardinality === 1) {
      $element['#type'] = 'fieldset';
      $element['#collapsible'] = TRUE;
      $element['#collapsed'] = FALSE;
    }

    $element['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#maxlength' => 1000,
      '#default_value' => $items[$delta]->title ?? NULL,
      '#required' => $this->fieldDefinition->isRequired(),
    ];
    $element['function_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Function name'),
      '#maxlength' => 100,
      '#default_value' => $items[$delta]->function_name ?? NULL,
      '#required' => $this->fieldDefinition->isRequired(),
    ];
    $element['function_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Function code'),
      '#maxlength' => 50,
      '#default_value' => $items[$delta]->function_code ?? NULL,
      '#required' => $this->fieldDefinition->isRequired(),
    ];
    $element['function_weight'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Function precedence'),
      '#maxlength' => 11,
      '#default_value' => $items[$delta]->function_weight ?? NULL,
      '#element_validate' => [
        [Number::class, 'validateNumber'],
      ],
      '#required' => $this->fieldDefinition->isRequired(),
    ];
    $element['body_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Body short name'),
      '#maxlength' => 500,
      '#default_value' => $items[$delta]->body_name ?? NULL,
    ];
    $element['body_long_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Body long name'),
      '#maxlength' => 1000,
      '#default_value' => $items[$delta]->body_long_name ?? NULL,
    ];
    $element['body_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Body code'),
      '#maxlength' => 50,
      '#default_value' => $items[$delta]->body_code ?? NULL,
    ];
    $element['body_weight'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Body precedence'),
      '#maxlength' => 11,
      '#default_value' => $items[$delta]->body_weight ?? NULL,
      '#element_validate' => [
        [Number::class, 'validateNumber'],
      ],
    ];
    $element['bodytype_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Body type code'),
      '#maxlength' => 50,
      '#default_value' => $items[$delta]->bodytype_code ?? NULL,
    ];
    $element['bodytype_weight'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Body type precedence'),
      '#maxlength' => 11,
      '#default_value' => $items[$delta]->bodytype_weight ?? NULL,
      '#element_validate' => [
        [Number::class, 'validateNumber'],
      ],
    ];

    // Get the existing value, if any, for the entity reference field.
    $target_type = $this->getFieldSetting('target_type');
    $default = isset($items[$delta]) ? $items[$delta]->target_id : NULL;
    if (!empty($default)) {
      $default = $this->entityTypeManager
        ->getStorage($target_type)
        ->load($default);
    }
    $element['target_id'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Body node reference'),
      '#target_type' => $this->getFieldSetting('target_type'),
      '#selection_handler' => $this->getFieldSetting('handler'),
      '#selection_settings' => $this->getFieldSetting('handler_settings'),
      '#default_value' => $default ?? NULL,
    ];

    // As the widget has multiple fields, we need to use a custom
    // validation method to provide a better understanding error message.
    $element['#element_validate'][] = [
      static::class,
      'validateElement',
    ];

    return $element;
  }

  /**
   * Form validation handler for widget elements.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public static function validateElement(array $element, FormStateInterface $form_state) {
    if (!empty($element['#required'])) {
      if (empty($element['title']['#value'])) {
        $form_state->setError($element['title'], t('The Title of the @name field is required.', ['@name' => $element['#title']]));
      }

      if (empty($element['function_name']['#value'])) {
        $form_state->setError($element['function_name'], t('The Function name of the @name field is required.', ['@name' => $element['#title']]));
      }

      if (empty($element['function_code']['#value'])) {
        $form_state->setError($element['function_code'], t('The Function code of the @name field is required.', ['@name' => $element['#title']]));
      }

      if (empty($element['function_weight']['#value']) && ($element['function_weight']['#value'] != '0')) {
        $form_state->setError($element['function_weight'], t('The Function precedence of the @name field is required.', ['@name' => $element['#title']]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    // First we build the list of fields that can cause this issue: optional
    // integers or references fields.
    $field_definition = $this->fieldDefinition->getFieldStorageDefinition();
    $property_definitions = $field_definition->getPropertyDefinitions();
    $to_unset = [];
    foreach ($property_definitions as $field_name => $property_definition) {
      if (!$property_definition->isRequired() &&
        ($property_definition instanceof DataReferenceTargetDefinition ||
          ($property_definition instanceof DataDefinition && $property_definition->getDataType() === 'integer'))) {
        $to_unset[] = $field_name;
      }
    }

    // Then we check our form values to see if these fields are empty to safely
    // unset them and prevent the apparition of the error.
    $values = parent::massageFormValues($values, $form, $form_state);
    foreach ($values as $delta => $value) {
      foreach ($value as $field_name => $field_value) {
        if (empty($field_value) && in_array($field_name, $to_unset, FALSE)) {
          unset($values[$delta][$field_name]);
        }
      }
    }
    return $values;
  }

}
