<?php

namespace Drupal\Tests\epp_custom_fields\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Tests epp_contact field behavior.
 *
 * @group epp_custom_fields
 */
class EPPContactTest extends EPPFieldTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $field_definition = FieldStorageConfig::create([
      'field_name' => 'field_epp_contact',
      'type' => 'epp_contact',
      'entity_type' => 'entity_test',
      'cardinality' => 2,
      'translatable' => TRUE,
    ]);
    $field_definition->save();

    $instance = FieldConfig::create([
      'field_name' => 'field_epp_contact',
      'label' => 'An EPPContact field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ]);
    $instance->save();

    $this->fieldConfig = \Drupal::entityTypeManager()
      ->getStorage('field_config')
      ->load('entity_test.entity_test.field_epp_contact');
  }

  /**
   * Test field schema.
   */
  public function testFieldSchema() {
    $schema = $this->fieldConfig->getFieldStorageDefinition()->getSchema();

    $columns = array_keys($schema['columns']);

    $this->assertEquals([
      'location',
      'office',
      'address',
      'phone',
      'mobile',
      'fax',
    ], $columns);
  }

}
