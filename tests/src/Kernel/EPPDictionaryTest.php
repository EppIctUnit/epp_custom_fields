<?php

namespace Drupal\Tests\epp_custom_fields\Kernel;

use Drupal\Core\Language\LanguageInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\media\Entity\MediaType;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Tests epp_dictionary field behavior.
 *
 * @group epp_custom_fields
 */
class EPPDictionaryTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'epp_custom_fields',
    'media',
    'taxonomy',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['media', 'taxonomy']);

    $mediaType = MediaType::create([
      'id' => 'photo',
      'label' => 'Photo',
      'source' => 'image',
    ]);
    $mediaType->save();

    $vocabulary = Vocabulary::create([
      'name' => 'Category',
      'description' => '',
      'vid' => 'topics',
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
      'weight' => 0,
    ]);
    $vocabulary->save();

    $field_definition = FieldStorageConfig::create([
      'field_name' => 'field_epp_dictionary',
      'type' => 'epp_dictionary',
      'entity_type' => 'media',
      'settings' => [
        'target_type' => 'taxonomy_term',
      ],
      'translatable' => TRUE,
      'cardinality' => 2,
    ]);
    $field_definition->save();

    $instance = FieldConfig::create([
      'field_name' => 'field_epp_dictionary',
      'label' => 'An EPPDictionary field',
      'entity_type' => 'media',
      'bundle' => 'photo',
      'settings' => [
        'handler' => 'default',
        'handler_settings' => [
          'target_bundles' => [
            'topics' => 'topics',
          ],
        ],
      ],
    ]);
    $instance->save();
  }

  /**
   * Test field schema.
   */
  public function testEppDictionarySchema() {
    /** @var \Drupal\field\Entity\FieldConfig $field_config */
    $field_config = \Drupal::entityTypeManager()
      ->getStorage('field_config')
      ->load('media.photo.field_epp_dictionary');
    $schema = $field_config->getFieldStorageDefinition()->getSchema();

    $columns = array_keys($schema['columns']);

    $this->assertEquals([
      'stid',
      'target_id',
    ], $columns);
  }

  /**
   * Test field target type and authorized bundles.
   */
  public function testEppDictionaryAuthorizedBundles() {
    /** @var \Drupal\field\Entity\FieldConfig $field_config */
    $field_config = \Drupal::entityTypeManager()
      ->getStorage('field_config')
      ->load('media.photo.field_epp_dictionary');
    $settings = $field_config->getSettings();

    $targetBundle = reset($settings['handler_settings']['target_bundles']);

    $this->assertEquals([
      'taxonomy_term',
      'topics',
    ], [
      $settings['target_type'],
      $targetBundle,
    ]);
  }

}
