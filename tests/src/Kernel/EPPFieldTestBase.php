<?php

namespace Drupal\Tests\epp_custom_fields\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Defines a base class for epp field kernel tests.
 */
abstract class EPPFieldTestBase extends KernelTestBase {

  use UserCreationTrait;

  /**
   * The field config.
   *
   * @var \Drupal\field\Entity\FieldConfig
   */
  protected $fieldConfig;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_test',
    'epp_custom_fields',
    'field',
    'user',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('entity_test');
    $this->setUpCurrentUser();
  }

  /**
   * Test field cardinality.
   */
  public function testFieldCardinality() {
    $cardinality = $this->fieldConfig->getFieldStorageDefinition()
      ->getCardinality();

    $this->assertEquals(2, $cardinality);
  }

  /**
   * Test field translatability.
   */
  public function testFieldTranslatability() {
    $translatability = $this->fieldConfig->getFieldStorageDefinition()
      ->isTranslatable();

    $this->assertEquals(TRUE, $translatability);
  }

}
