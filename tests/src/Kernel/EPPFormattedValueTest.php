<?php

namespace Drupal\Tests\epp_custom_fields\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Tests epp_formatted_value field behavior.
 *
 * @group epp_custom_fields
 */
class EPPFormattedValueTest extends EPPFieldTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $field_definition = FieldStorageConfig::create([
      'field_name' => 'field_epp_formatted_value',
      'type' => 'epp_formatted_value',
      'entity_type' => 'entity_test',
      'cardinality' => 2,
      'translatable' => TRUE,
    ]);
    $field_definition->save();

    $instance = FieldConfig::create([
      'field_name' => 'field_epp_formatted_value',
      'label' => 'An EPPFormattedValue field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ]);
    $instance->save();

    $this->fieldConfig = \Drupal::entityTypeManager()
      ->getStorage('field_config')
      ->load('entity_test.entity_test.field_epp_formatted_value');
  }

  /**
   * Test field schema.
   */
  public function testFieldSchema() {
    $schema = $this->fieldConfig->getFieldStorageDefinition()->getSchema();

    $columns = array_keys($schema['columns']);

    $this->assertEquals([
      'value',
      'format',
      'safe_value',
    ], $columns);
  }

}
