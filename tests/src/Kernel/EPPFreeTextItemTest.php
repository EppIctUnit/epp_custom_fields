<?php

namespace Drupal\Tests\epp_custom_fields\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Tests epp_free_text_item field behavior.
 *
 * @group epp_custom_fields
 */
class EPPFreeTextItemTest extends EPPFieldTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $field_definition = FieldStorageConfig::create([
      'field_name' => 'field_epp_free_text_item',
      'type' => 'epp_free_text_item',
      'entity_type' => 'entity_test',
      'cardinality' => 2,
      'translatable' => TRUE,
    ]);
    $field_definition->save();

    $instance = FieldConfig::create([
      'field_name' => 'field_epp_free_text_item',
      'label' => 'An EPPFreeTextItem field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ]);
    $instance->save();

    $this->fieldConfig = \Drupal::entityTypeManager()
      ->getStorage('field_config')
      ->load('entity_test.entity_test.field_epp_free_text_item');
  }

  /**
   * Test field schema.
   */
  public function testFieldSchema() {
    $schema = $this->fieldConfig->getFieldStorageDefinition()->getSchema();

    $columns = array_keys($schema['columns']);

    $this->assertEquals([
      'description',
      'text',
      'text_format',
    ], $columns);
  }

}
