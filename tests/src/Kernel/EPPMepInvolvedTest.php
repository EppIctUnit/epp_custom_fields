<?php

namespace Drupal\Tests\epp_custom_fields\Kernel;

use Drupal\Core\Language\LanguageInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\media\Entity\MediaType;
use Drupal\node\Entity\NodeType;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Tests epp_mep_involved field behavior.
 *
 * @group epp_custom_fields
 */
class EPPMepInvolvedTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'epp_custom_fields',
    'media',
    'node',
    'taxonomy',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['media', 'node', 'taxonomy']);

    $mediaType = MediaType::create([
      'id' => 'photo',
      'label' => 'Photo',
      'source' => 'image',
    ]);
    $mediaType->save();

    $vocabulary = Vocabulary::create([
      'name' => 'MEPs Involved Roles',
      'description' => '',
      'vid' => 'meps_involved_roles',
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
      'weight' => 0,
    ]);
    $vocabulary->save();

    $nodeType = NodeType::create(['type' => 'mep', 'name' => 'Mep gallery']);
    $nodeType->save();

    $field_definition = FieldStorageConfig::create([
      'field_name' => 'field_epp_mep_involved',
      'type' => 'epp_mep_involved',
      'entity_type' => 'media',
      'settings' => [
        'role_target_type' => 'taxonomy_term',
        'node_target_type' => 'node',
      ],
      'translatable' => TRUE,
      'cardinality' => 2,
    ]);
    $field_definition->save();

    $instance = FieldConfig::create([
      'field_name' => 'field_epp_mep_involved',
      'label' => 'An EPPMepInvolved field',
      'entity_type' => 'media',
      'bundle' => 'photo',
      'settings' => [
        'role' => [
          'handler' => 'default',
          'handler_settings' => [
            'target_bundles' => [
              'meps_involved_roles' => 'meps_involved_roles',
            ],
          ],
        ],
        'node' => [
          'handler' => 'default',
          'handler_settings' => [
            'target_bundles' => [
              'mep' => 'mep',
            ],
          ],
        ],
      ],
    ]);
    $instance->save();
  }

  /**
   * Test field schema.
   */
  public function testEppMepInvolvedSchema() {
    /** @var \Drupal\field\Entity\FieldConfig $field_config */
    $field_config = \Drupal::entityTypeManager()
      ->getStorage('field_config')
      ->load('media.photo.field_epp_mep_involved');
    $schema = $field_config->getFieldStorageDefinition()->getSchema();

    $columns = array_keys($schema['columns']);

    $this->assertEquals([
      'role_code',
      'role_target_id',
      'node_target_id',
    ], $columns);
  }

  /**
   * Test field target type and authorized bundles.
   */
  public function testEppMepInvolvedAuthorizedBundles() {
    /** @var \Drupal\field\Entity\FieldConfig $field_config */
    $field_config = \Drupal::entityTypeManager()
      ->getStorage('field_config')
      ->load('media.photo.field_epp_mep_involved');
    $settings = $field_config->getSettings();

    $targetNodeBundle = reset($settings['node']['handler_settings']['target_bundles']);
    $targetRoleBundle = reset($settings['role']['handler_settings']['target_bundles']);

    $this->assertEquals([
      'taxonomy_term',
      'meps_involved_roles',
      'node',
      'mep',
    ], [
      $settings['role_target_type'],
      $targetRoleBundle,
      $settings['node_target_type'],
      $targetNodeBundle,
    ]);
  }

}
