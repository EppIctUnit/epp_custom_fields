<?php

namespace Drupal\Tests\epp_custom_fields\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Tests epp_mep_role field behavior.
 *
 * @group epp_custom_fields
 */
class EPPMepRoleTest extends EPPFieldTestBase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $field_definition = FieldStorageConfig::create([
      'field_name' => 'field_epp_mep_role',
      'type' => 'epp_mep_role',
      'entity_type' => 'entity_test',
      'cardinality' => 2,
      'translatable' => TRUE,
    ]);
    $field_definition->save();

    $instance = FieldConfig::create([
      'field_name' => 'field_epp_mep_role',
      'label' => 'An EPPMepRole field',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ]);
    $instance->save();

    $this->fieldConfig = \Drupal::entityTypeManager()
      ->getStorage('field_config')
      ->load('entity_test.entity_test.field_epp_mep_role');
  }

  /**
   * Test field schema.
   */
  public function testFieldSchema() {
    $schema = $this->fieldConfig->getFieldStorageDefinition()->getSchema();

    $columns = array_keys($schema['columns']);

    $this->assertEquals([
      'code',
      'name',
      'weight',
    ], $columns);
  }

}
