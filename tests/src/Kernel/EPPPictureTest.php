<?php

namespace Drupal\Tests\epp_custom_fields\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\media\Entity\MediaType;
use Drupal\node\Entity\NodeType;

/**
 * Tests epp_picture field behavior.
 *
 * @group epp_custom_fields
 */
class EPPPictureTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'epp_custom_fields',
    'media',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['media', 'node']);

    $nodeType = NodeType::create([
      'type' => 'photo_gallery',
      'name' => 'Photo gallery',
    ]);
    $nodeType->save();

    $mediaType = MediaType::create([
      'id' => 'photo',
      'label' => 'Photo',
      'source' => 'image',
    ]);
    $mediaType->save();

    $field_definition = FieldStorageConfig::create([
      'field_name' => 'field_epp_picture',
      'type' => 'epp_picture',
      'entity_type' => 'node',
      'settings' => [
        'target_type' => 'media',
      ],
      'translatable' => TRUE,
      'cardinality' => 2,
    ]);
    $field_definition->save();

    $instance = FieldConfig::create([
      'field_name' => 'field_epp_picture',
      'label' => 'An EPPPicture field',
      'entity_type' => 'node',
      'bundle' => 'photo_gallery',
      'settings' => [
        'handler' => 'default',
        'handler_settings' => [
          'target_bundles' => [
            'photo' => 'photo',
          ],
        ],
      ],
    ]);
    $instance->save();
  }

  /**
   * Test field schema.
   */
  public function testEppPictureSchema() {
    /** @var \Drupal\field\Entity\FieldConfig $field_config */
    $field_config = \Drupal::entityTypeManager()
      ->getStorage('field_config')
      ->load('node.photo_gallery.field_epp_picture');
    $schema = $field_config->getFieldStorageDefinition()->getSchema();

    $columns = array_keys($schema['columns']);

    $this->assertEquals([
      'alternate',
      'description',
      'description_format',
      'target_id',
      'fid',
    ], $columns);
  }

  /**
   * Test field target type and authorized bundles.
   */
  public function testEppPictureAuthorizedBundles() {
    /** @var \Drupal\field\Entity\FieldConfig $field_config */
    $field_config = \Drupal::entityTypeManager()
      ->getStorage('field_config')
      ->load('node.photo_gallery.field_epp_picture');
    $settings = $field_config->getSettings();

    $targetBundle = reset($settings['handler_settings']['target_bundles']);

    $this->assertEquals([
      'media',
      'photo',
    ], [
      $settings['target_type'],
      $targetBundle,
    ]);
  }

}
