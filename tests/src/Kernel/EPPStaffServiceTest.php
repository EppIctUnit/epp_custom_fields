<?php

namespace Drupal\Tests\epp_custom_fields\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\Entity\NodeType;

/**
 * Tests epp_staff_service field behavior.
 *
 * @group epp_custom_fields
 */
class EPPStaffServiceTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'epp_custom_fields',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $staffNodeType = NodeType::create(['type' => 'staff', 'name' => 'Staff']);
    $staffNodeType->save();

    $committeeNodeType = NodeType::create([
      'type' => 'committee',
      'name' => 'Committee',
    ]);
    $committeeNodeType->save();

    $interparliamentaryDelegationNodeType = NodeType::create([
      'type' => 'interparliamentary_delegation',
      'name' => 'Interparliamentary delegation',
    ]);
    $interparliamentaryDelegationNodeType->save();

    $nationalDelegationNodeType = NodeType::create([
      'type' => 'national_delegation',
      'name' => 'National delegation',
    ]);
    $nationalDelegationNodeType->save();

    $field_definition = FieldStorageConfig::create([
      'field_name' => 'field_epp_staff_service',
      'type' => 'epp_staff_service',
      'entity_type' => 'node',
      'settings' => [
        'target_type' => 'node',
      ],
      'translatable' => TRUE,
      'cardinality' => 2,
    ]);
    $field_definition->save();

    $instance = FieldConfig::create([
      'field_name' => 'field_epp_staff_service',
      'label' => 'A EPPStaffService field',
      'entity_type' => 'node',
      'bundle' => 'staff',
      'settings' => [
        'handler' => 'default',
        'handler_settings' => [
          'target_bundles' => [
            'committee' => 'committee',
            'interparliamentary_delegation' => 'interparliamentary_delegation',
            'national_delegation' => 'national_delegation',
          ],
        ],
      ],
    ]);
    $instance->save();
  }

  /**
   * Test field schema.
   */
  public function testEppStaffServiceSchema() {
    /** @var \Drupal\field\Entity\FieldConfig $field_config */
    $field_config = \Drupal::entityTypeManager()
      ->getStorage('field_config')
      ->load('node.staff.field_epp_staff_service');
    $schema = $field_config->getFieldStorageDefinition()->getSchema();

    $columns = array_keys($schema['columns']);

    $this->assertEquals([
      'short_name',
      'long_name',
      'type',
      'service_code',
      'body_code',
      'target_id',
      'important',
      'responsible',
      'precedence',
      'internet_title',
      'internal_title',
    ], $columns);
  }

  /**
   * Test field target type and authorized bundles.
   */
  public function testEppStaffServiceAuthorizedBundles() {
    /** @var \Drupal\field\Entity\FieldConfig $field_config */
    $field_config = \Drupal::entityTypeManager()
      ->getStorage('field_config')
      ->load('node.staff.field_epp_staff_service');
    $settings = $field_config->getSettings();

    $this->assertEquals([
      'node',
      [
        'committee' => 'committee',
        'interparliamentary_delegation' => 'interparliamentary_delegation',
        'national_delegation' => 'national_delegation',
      ],
    ], [
      $settings['target_type'],
      $settings['handler_settings']['target_bundles'],
    ]);
  }

}
