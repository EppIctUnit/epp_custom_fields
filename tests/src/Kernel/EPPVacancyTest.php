<?php

namespace Drupal\Tests\epp_custom_fields\Kernel;

use Drupal\Core\Language\LanguageInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\Entity\NodeType;
use Drupal\taxonomy\Entity\Vocabulary;

/**
 * Tests epp_vacancy field behavior.
 *
 * @group epp_custom_fields
 */
class EPPVacancyTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'epp_custom_fields',
    'node',
    'taxonomy',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['node', 'taxonomy']);

    $gradeVocabulary = Vocabulary::create([
      'name' => 'Grade',
      'description' => '',
      'vid' => 'grade',
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
      'weight' => 0,
    ]);
    $gradeVocabulary->save();

    $positionTypeVocabulary = Vocabulary::create([
      'name' => 'Position type',
      'description' => '',
      'vid' => 'position_type',
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
      'weight' => 0,
    ]);
    $positionTypeVocabulary->save();

    $nodeType = NodeType::create(['type' => 'vacancies', 'name' => 'Vacancy']);
    $nodeType->save();

    $field_definition = FieldStorageConfig::create([
      'field_name' => 'field_epp_vacancy',
      'type' => 'epp_vacancy',
      'entity_type' => 'node',
      'settings' => [
        'grade_target_type' => 'taxonomy_term',
        'position_type_target_type' => 'taxonomy_term',
      ],
      'translatable' => TRUE,
      'cardinality' => 2,
    ]);
    $field_definition->save();

    $instance = FieldConfig::create([
      'field_name' => 'field_epp_vacancy',
      'label' => 'An EPPVacancy field',
      'entity_type' => 'node',
      'bundle' => 'vacancies',
      'settings' => [
        'grade' => [
          'handler' => 'default',
          'handler_settings' => [
            'target_bundles' => [
              'grade' => 'grade',
            ],
          ],
        ],
        'position_type' => [
          'handler' => 'default',
          'handler_settings' => [
            'target_bundles' => [
              'position_type' => 'position_type',
            ],
          ],
        ],
      ],
    ]);
    $instance->save();
  }

  /**
   * Test field schema.
   */
  public function testEppVacancySchema() {
    /** @var \Drupal\field\Entity\FieldConfig $field_config */
    $field_config = \Drupal::entityTypeManager()
      ->getStorage('field_config')
      ->load('node.vacancies.field_epp_vacancy');
    $schema = $field_config->getFieldStorageDefinition()->getSchema();

    $columns = array_keys($schema['columns']);

    $this->assertEquals([
      'reference',
      'grade_target_id',
      'description',
      'position_type_target_id',
      'deadline',
      'apply_link',
    ], $columns);
  }

  /**
   * Test field target type and authorized bundles.
   */
  public function testEppVacancyAuthorizedBundles() {
    /** @var \Drupal\field\Entity\FieldConfig $field_config */
    $field_config = \Drupal::entityTypeManager()
      ->getStorage('field_config')
      ->load('node.vacancies.field_epp_vacancy');
    $settings = $field_config->getSettings();

    $targetGradeBundle = reset($settings['grade']['handler_settings']['target_bundles']);
    $targetPositionTypeBundle = reset($settings['position_type']['handler_settings']['target_bundles']);

    $this->assertEquals([
      'taxonomy_term',
      'grade',
      'taxonomy_term',
      'position_type',
    ], [
      $settings['grade_target_type'],
      $targetGradeBundle,
      $settings['position_type_target_type'],
      $targetPositionTypeBundle,
    ]);
  }

}
