<?php

namespace Drupal\Tests\epp_custom_fields\Kernel;

use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\node\Entity\NodeType;

/**
 * Tests mep_title field behavior.
 *
 * @group epp_custom_fields
 */
class MEPTitleTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'epp_custom_fields',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $mepNodeType = NodeType::create(['type' => 'mep', 'name' => 'Mep']);
    $mepNodeType->save();

    $committeeNodeType = NodeType::create([
      'type' => 'committee',
      'name' => 'Committee',
    ]);
    $committeeNodeType->save();

    $interparliamentaryDelegationNodeType = NodeType::create([
      'type' => 'interparliamentary_delegation',
      'name' => 'Interparliamentary delegation',
    ]);
    $interparliamentaryDelegationNodeType->save();

    $nationalDelegationNodeType = NodeType::create([
      'type' => 'national_delegation',
      'name' => 'National delegation',
    ]);
    $nationalDelegationNodeType->save();

    $partyNodeType = NodeType::create([
      'type' => 'party',
      'name' => 'Party',
    ]);
    $partyNodeType->save();

    $workingGroupNodeType = NodeType::create([
      'type' => 'working_group',
      'name' => 'Working group',
    ]);
    $workingGroupNodeType->save();

    $field_definition = FieldStorageConfig::create([
      'field_name' => 'field_mep_title',
      'type' => 'mep_title',
      'entity_type' => 'node',
      'settings' => [
        'target_type' => 'node',
      ],
      'translatable' => TRUE,
      'cardinality' => 2,
    ]);
    $field_definition->save();

    $instance = FieldConfig::create([
      'field_name' => 'field_mep_title',
      'label' => 'A MEPTitle field',
      'entity_type' => 'node',
      'bundle' => 'mep',
      'settings' => [
        'handler' => 'default',
        'handler_settings' => [
          'target_bundles' => [
            'committee' => 'committee',
            'interparliamentary_delegation' => 'interparliamentary_delegation',
            'national_delegation' => 'national_delegation',
            'party' => 'party',
            'working_group' => 'working_group',
          ],
        ],
      ],
    ]);
    $instance->save();
  }

  /**
   * Test field schema.
   */
  public function testMeptitleSchema() {
    /** @var \Drupal\field\Entity\FieldConfig $field_config */
    $field_config = \Drupal::entityTypeManager()
      ->getStorage('field_config')
      ->load('node.mep.field_mep_title');
    $schema = $field_config->getFieldStorageDefinition()->getSchema();

    $columns = array_keys($schema['columns']);

    $this->assertEquals([
      'title',
      'function_name',
      'function_code',
      'function_weight',
      'body_name',
      'body_long_name',
      'body_code',
      'body_weight',
      'bodytype_code',
      'bodytype_weight',
      'target_id',
    ], $columns);
  }

  /**
   * Test field target type and authorized bundles.
   */
  public function testMepTitleAuthorizedBundles() {
    /** @var \Drupal\field\Entity\FieldConfig $field_config */
    $field_config = \Drupal::entityTypeManager()
      ->getStorage('field_config')
      ->load('node.mep.field_mep_title');
    $settings = $field_config->getSettings();

    $this->assertEquals([
      'node',
      [
        'committee' => 'committee',
        'interparliamentary_delegation' => 'interparliamentary_delegation',
        'national_delegation' => 'national_delegation',
        'party' => 'party',
        'working_group' => 'working_group',
      ],
    ], [
      $settings['target_type'],
      $settings['handler_settings']['target_bundles'],
    ]);
  }

}
